<?php

use App\Http\Controllers\ArtisanController;
use App\Http\Controllers\BotUserInfoController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\DriverStatusController;
use App\Http\Controllers\Dsp\LoadController as DspLoadController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\LoadController;
use App\Http\Controllers\PaymentGatewayController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\StatsController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TelegramBotController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TrailerController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\IsAdmin;
use App\Http\Middleware\IsDsp;
use App\Http\Middleware\IsManager;
use App\Http\Middleware\IsQc;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// telegram bot
Route::post('telegram-bot-r91zh779lfm7h8tq', [TelegramBotController::class, 'index'])->name('telegram-bot');
Route::get('telegram-bot/sethook-r91zh779lfm7h8tq', [TelegramBotController::class, 'sethook'])->name('telegram-bot.sethook');
Route::get('telegram-bot/deletehook-r91zh779lfm7h8tq', [TelegramBotController::class, 'deletehook'])->name('telegram-bot.deletehook');

// Payment
Route::post('paycom-emkooolkztfbzciw', [PaymentGatewayController::class, 'paycom'])->name('payment-gateway.paycom');
Route::any('click-pnhhqhfniksaejcn/prepare', [PaymentGatewayController::class, 'click'])->name('payment-gateway.click.prepare');
Route::any('click-pnhhqhfniksaejcn/complete', [PaymentGatewayController::class, 'click'])->name('payment-gateway.click.complete');

// Test
Route::get('test', [TestController::class, 'index']);

// report
Route::get('loads/report/daily-d778a0jdb29r', [LoadController::class, 'report'])->name('loads.report.daily.public');

Route::group(['middleware' => ['auth']], function () {

    // common routes
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('profile', [ProfileController::class, 'index'])->name('profile.index');
    Route::post('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    Route::post('profile/update/password', [ProfileController::class, 'updatePassword'])->name('profile.update.password');

    // admin routes
    Route::group(['middleware' => [IsAdmin::class]], function () {
        Route::get('stats/drivers', [StatsController::class, 'drivers'])->name('stats.drivers');
        Route::resource('bot-user-info', BotUserInfoController::class);
        Route::resource('teams', TeamController::class);
        Route::resource('trailers', TrailerController::class);
        Route::get('drivers/{driver}/stats', [DriverController::class, 'stats'])->name('drivers.stats');
        Route::get('drivers/{driver}/history', [DriverController::class, 'history'])->name('drivers.history');
        Route::resource('drivers', DriverController::class);
        Route::get('loads', [LoadController::class, 'index'])->name('loads.index');
        Route::delete('loads/{load}', [LoadController::class, 'destroy'])->name('loads.destroy');
        Route::get('loads/download', [LoadController::class, 'download'])->name('loads.download');
        Route::get('loads/report', [LoadController::class, 'report'])->name('loads.report');

        Route::get('driver-statuses', [DriverStatusController::class, 'index'])->name('driver-statuses.index');
        Route::get('driver-statuses/{driverStatus}/edit', [DriverStatusController::class, 'edit'])->name('driver-statuses.edit');
        Route::put('driver-statuses/{driverStatus}', [DriverStatusController::class, 'update'])->name('driver-statuses.update');
        Route::delete('driver-statuses/{driverStatus}', [DriverStatusController::class, 'destroy'])->name('driver-statuses.destroy');

        Route::resource('users', UserController::class);
        Route::post('images', [ImageController::class, 'store'])->name('images.store');
        Route::resource('settings', SettingController::class);
    });

    // manager routes
    Route::group(['middleware' => [IsManager::class]], function () {
        //
    });

    // dsp routes
    Route::group(['middleware' => [IsDsp::class]], function () {
        //
    });

    // qc routes
    Route::group(['middleware' => [IsQc::class]], function () {
        //
    });

    // common boards
    Route::get('board', [HomeController::class, 'board'])->name('board');
    Route::get('team-sheet', [HomeController::class, 'teamSheet'])->name('team-sheet');
    Route::get('kpi-board', [HomeController::class, 'kpiBoard'])->name('kpi-board');
    Route::get('drivers-board', [HomeController::class, 'driversBoard'])->name('drivers-board');

    // loads
    Route::get('loads/{load}/cancel', [LoadController::class, 'cancelCreate'])->name('loads.cancel');
    Route::post('loads/{load}/cancel', [LoadController::class, 'cancelStore']);
    Route::get('loads/{load}/edit/confirm', [LoadController::class, 'editConfirmCreate'])->name('loads.edit.confirm');
    Route::put('loads/{load}/edit/confirm', [LoadController::class, 'editConfirmStore']);
    Route::get('loads/{load}', [LoadController::class, 'show'])->name('loads.show');
    Route::get('loads/{load}/edit', [LoadController::class, 'edit'])->name('loads.edit');
    Route::put('loads/{load}', [LoadController::class, 'update'])->name('loads.update');

    // artisan
    Route::get('artisan/migrate', [ArtisanController::class, 'migrate']);
    Route::get('artisan/driver/status-update', [ArtisanController::class, 'driverStatusUpdate']);
    Route::get('artisan/driver/create-weekly-plan', [ArtisanController::class, 'driverCreateWeeklyPlan']);
});

Auth::routes(['register' => false, 'reset' => false]);
