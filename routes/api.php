<?php

use App\Http\Controllers\Api\DriverController;
use App\Http\Controllers\Api\PageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('update-table-info', [PageController::class, 'updateTableInfo'])->name('api.update-table-info');
    Route::get('drivers/{driver}/timeline', [DriverController::class, 'timeline'])->name('api.drivers.timeline');
});

// Route::get('attributes', 'Api\AttributesController@index');
// Route::get('attributes/{attribute}', 'Api\AttributesController@show');
// Route::get('attributes/{attribute}/attribute-values', 'Api\AttributesController@attributeValues');

// Route::get('attribute-values', 'Api\AttributeValuesController@index');
// Route::get('attribute-values/{attributeValue}', 'Api\AttributeValuesController@show');
