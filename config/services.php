<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'telegram' => [
        'bot_name' => env('TELEGRAM_BOT_NAME'),
        'bot_token' => env('TELEGRAM_BOT_TOKEN'),
        'hook_url' => env('TELEGRAM_HOOK_URL'),
        'chat_id' => env('TELEGRAM_CHAT_ID'),
        'admin_chat_id' => env('TELEGRAM_ADMIN_CHAT_ID'),
    ],

    'paycom' => [
	    'merchant_id' => env('PAYCOM_MERCHANT_ID', ''),
	    'login'       => 'Paycom',
	    'keyFile'     => env('PAYCOM_ENV', 'test') == 'production' ? config_path('password.paycom') : config_path('password-test.paycom'),
        'telegram_payment_provider_token' => env('PAYCOM_TELEGRAM_PAYMENT_PROVIDER_TOKEN', ''),
	],

    'click' => [
        'merchant_id' => env('CLICK_MERCHANT_ID', ''),
        'service_id' => env('CLICK_SERVICE_ID', ''),
        'user_id' => env('CLICK_USER_ID', ''),
        'secret_key' => env('CLICK_SECRET_KEY', ''),
        'telegram_payment_provider_token' => env('CLICK_TELEGRAM_PAYMENT_PROVIDER_TOKEN', ''),
        'model' => '\App\Models\Order',
    ],

    'cargoprime' => [
        'start_year' => env('CARGOPRIME_START_YEAR', 2023),
    ],

];
