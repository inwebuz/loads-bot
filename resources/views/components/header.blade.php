<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm d-print-none">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('home') }}">
            {{ config('app.name', '') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            @auth
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('board') }}">{{ __('Board') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('team-sheet') }}">{{ __('Team sheet') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('kpi-board') }}">{{ __('KPI board') }}</a>
                    </li>
                    @if (Auth::user()->isAdmin())
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('drivers-board') }}">{{ __('Drivers board') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('loads.index') }}">{{ __('Loads') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('drivers.index') }}">{{ __('Drivers') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('driver-statuses.index') }}">{{ __('Driver statuses') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('trailers.index') }}">{{ __('Trailers') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('teams.index') }}">{{ __('Teams') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('bot-user-info.index') }}">{{ __('Bot users') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('users.index') }}">{{ __('Users') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('settings.index') }}">{{ __('Settings') }}</a>
                        </li>
                    @elseif (Auth::user()->isManager())
                    @elseif (Auth::user()->isQc())
                    @endif
                </ul>
            @endauth

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">

                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} | {{ Auth::user()->role_name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('profile.index') }}">
                                {{ __('Profile') }}
                            </a>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
