@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-header">{{ __('Dashboard') }}</div>
                    <div class="card-body">
                        @include('partials.alerts')
                        <div class="my-4">
                            <h3>{{ Auth::user()->name }} | {{ Auth::user()->role_name }}</h3>
                            @php
                                $botUserInfo = auth()->user()->botUserInfo;
                            @endphp
                            @if ($botUserInfo)
                                <div>
                                    {{ __('Position') }}: {{ $botUserInfo->position }}
                                </div>
                                <div>
                                    {{ __('Shift') }}: {{ $botUserInfo->shift }}
                                </div>
                                <div>
                                    {{ __('Team') }}: {{ $botUserInfo->team_name }}
                                </div>
                            @endif
                        </div>
                        @auth
                            <div class="my-4">
                                @if (Auth::user()->isAdmin())
                                    <a class="btn btn-lg btn-info mb-1" href="{{ route('board') }}">{{ __('Board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('team-sheet') }}">{{ __('Team sheet') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('kpi-board') }}">{{ __('KPI board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('drivers-board') }}">{{ __('Drivers board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('loads.index') }}">{{ __('Loads') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('drivers.index') }}">{{ __('Drivers') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('driver-statuses.index') }}">{{ __('Driver statuses') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('trailers.index') }}">{{ __('Trailers') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('teams.index') }}">{{ __('Teams') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('bot-user-info.index') }}">{{ __('Bot users') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('users.index') }}">{{ __('Users') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('settings.index') }}">{{ __('Settings') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('profile.index') }}">{{ __('Profile') }}</a>
                                @elseif (Auth::user()->isManager())
                                    <a class="btn btn-lg btn-info mb-1" href="{{ route('board') }}">{{ __('Board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('team-sheet') }}">{{ __('Team sheet') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('kpi-board', ['team_id' => Auth::user()->botUserInfo->team_id ?? '']) }}">{{ __('KPI board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('profile.index') }}">{{ __('Profile') }}</a>
                                @elseif (Auth::user()->isDsp())
                                    <a class="btn btn-lg btn-info mb-1" href="{{ route('board') }}">{{ __('Board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('team-sheet') }}">{{ __('Team sheet') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('kpi-board', ['dispatcher_id' => Auth::user()->botUserInfo->id ?? '']) }}">{{ __('KPI board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('profile.index') }}">{{ __('Profile') }}</a>
                                @elseif (Auth::user()->isUser())
                                    <a class="btn btn-lg btn-info mb-1" href="{{ route('board') }}">{{ __('Board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('team-sheet') }}">{{ __('Team sheet') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('kpi-board') }}">{{ __('KPI board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('profile.index') }}">{{ __('Profile') }}</a>
                                @elseif (Auth::user()->isQc())
                                    <a class="btn btn-lg btn-info mb-1" href="{{ route('board') }}">{{ __('Board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('team-sheet') }}">{{ __('Team sheet') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('kpi-board') }}">{{ __('KPI board') }}</a>
                                    <a class="btn btn-lg btn-info mb-1"
                                        href="{{ route('profile.index') }}">{{ __('Profile') }}</a>
                                @endif
                            </div>
                        @endauth
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-header">{{ __('Driver group hashtags') }}</div>
                    <div class="card-body">
                        @foreach (Helper::groupHashtagMessages() as $key => $value)
                        <div class="mb-3">
                            <strong>{{ $key }}</strong> <br>
                            {!! nl2br($value) !!}
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
