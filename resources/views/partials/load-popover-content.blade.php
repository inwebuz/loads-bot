<div><strong>{{ __('DSP') }}:</strong> {{ $load->dsp_full_name }}</div>
<div><strong>{{ __('Type') }}:</strong> {{ $load->type_title }}</div>
<div><strong>{{ __('Company code') }}:</strong> {{ $load->company_code }}</div>
<div><strong>{{ __('Load ID') }}:</strong> {{ $load->load_id }}</div>
<div><strong>{{ __('PCS number') }}:</strong> {{ $load->pcs_number }}</div>

<div><strong>{{ __('Total $') }}:</strong> {{ $load->total }}</div>
<div><strong>{{ __('Rate') }}:</strong> {{ $load->rate }}</div>

<div><strong>{{ __('Total mile') }}:</strong> {{ $load->mile }}</div>
<div><strong>{{ __('Authorized loaded mile') }}:</strong> {{ $load->authorized_loaded_mile }}</div>
<div><strong>{{ __('Load DH mile') }}:</strong> {{ $load->deadhead_mile }}</div>
<div><strong>{{ __('Authorized mile (PLD, TRL, FLT miles)') }}:</strong> {{ $load->authorized_mile }}</div>
<div><strong>{{ __('Unauthorized miles (including Home DH miles)') }}:</strong> {{ $load->unauthorized_mile }}</div>

<div><strong>{{ __('PU address (full)') }}:</strong> {{ $load->pickup_location }}</div>
<div><strong>{{ __('PU time') }}:</strong> {{ $load->pickup_time }}</div>
<div><strong>{{ __('Delivery address (full)') }}:</strong> {{ $load->delivery_location }}</div>
<div><strong>{{ __('Delivery time') }}:</strong> {{ $load->delivery_time }}</div>
<div><strong>{{ __('Created') }}:</strong> {{ Helper::formatDateTime($load->created_at) }}</div>
@if ($load->isCancelled())
    <div class='text-danger'>{{ $load->status_title }}</div>
    <div><strong>{{ __('TONU') }}:</strong> {{ $load->tonu }}</div>
    <div><strong>{{ __('Cancel comment') }}:</strong> {{ $load->cancel_comment }}</div>
@endif
@if (!$load->isThirdParty())
    <div><strong>{{ __('Driver name') }}:</strong> {{ $load->driver_name }}</div>
@endif
@if ($load->isThirdParty())
    <div><strong>{{ __('Third company name') }}:</strong> {{ $load->third_company }}</div>
@endif
<div><strong>{{ __('Special note') }}:</strong> {{ $load->special_note }}</div>

<div><strong>{{ __('Check in as') }}:</strong> {{ $load->check_in_as }}</div>
<div><strong>{{ __('Trailer') }}:</strong> {{ $load->trailer }}</div>
<div><strong>{{ __('Trailer note') }}:</strong> {{ $load->trailer_note }}</div>
<div><strong>{{ __('Pickup address and time (2, 3, 4 Stops)') }}:</strong> {{ $load->pickup_additional_stops }}</div>

<div>
    <a href='{{ route('loads.show', [$load->id]) }}' target='_blank'>{{ __('Show') }}</a>
    @can('update-load', $load)
        <a href='{{ route('loads.edit', [$load->id]) }}' class='text-info' target='_blank'>{{ __('Edit') }}</a>
    @endcan
    @can('cancel-load', $load)
        <a href='{{ route('loads.cancel', [$load->id]) }}' class='text-danger' target='_blank'>{{ __('Cancel') }}</a>
    @endcan
</div>
