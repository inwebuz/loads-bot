<div class="modal" tabindex="-1" id="edit-table-info-modal">
    <div class="modal-dialog modal-dialog-centered">
        @csrf
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('api.update-table-info') }}" method="POST"
                    class="edit-table-info-form">
                    @csrf
                    <input type="hidden" name="target_selector" value="">
                    <input type="hidden" name="table" value="">
                    <input type="hidden" name="field" value="">
                    <input type="hidden" name="id" value="">
                    <textarea name="description" class="form-control" required></textarea>
                    <div class="mt-4">
                        <button type="submit" class="btn btn-primary inline-flex align-items-center">
                            <span>Save changes</span>
                        </button>
                    </div>
                    <div class="form-result my-2"></div>
                </form>
            </div>
        </div>
    </div>
</div>
