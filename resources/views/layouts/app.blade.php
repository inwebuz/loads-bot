<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @php
        $assetsVersion = env('ASSETS_VERSION', 2);
    @endphp
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


</head>
<body class="d-flex flex-column @yield('body_class')">
    <div id="page-content">
        <x-header></x-header>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <x-footer></x-footer>

    <a href="javascript:;" id="to-top-btn" class="d-none w-30 h-30 rounded-circle position-fixed r-15 b-15 align-items-center justify-content-center bg-primary text-white">
        <i class="fa fa-angle-up"></i>
    </a>

    @include('partials.modals.edit-table-info-modal')

    @yield('bottom')

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>
