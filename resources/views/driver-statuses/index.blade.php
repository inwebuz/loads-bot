@extends('layouts.app')

@section('title', __('Driver statuses'))

@section('content')
    <div class="container-fluid">

        @include('partials.alerts')

        <h1>{{ __('Driver statuses') }}</h1>

        <div class="my-4">
            <form action="{{ route('driver-statuses.index') }}" id="driver-statuses-filter-form">

                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="form_id">{{ __('ID') }}</label>
                                <input type="text" id="form_id" name="id" class="form-control"
                                    value="{{ $filter['id'] }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_driver_id">{{ __('Driver') }}</label>
                            <select class="form-control selectpicker" data-live-search="true" name="driver_id" id="form_driver_id">
                                <option value="-">-</option>
                                @foreach ($drivers as $driver)
                                    <option value="{{ $driver->id }}" @if ((string) $driver->id == $filter['driver_id']) selected @endif>
                                        {{ $driver->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_status">{{ __('Status') }}</label>
                            <select class="form-control" name="status" id="form_status">
                                <option value="-">-</option>
                                @foreach ($statuses as $key => $value)
                                    <option value="{{ $key }}" @if ((string) $key == $filter['status']) selected @endif>
                                        {{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ __('Apply filter') }}</button>
                </div>
            </form>
        </div>

        <div class="my-4">
            <div class="table-responsive">
                <table class="table table-bordered table-light table-hover">
                    <tr class="table-active">
                        <th>
                            {{ __('ID') }}
                        </th>
                        <th>
                            {{ __('Driver') }}
                        </th>
                        <th>
                            {{ __('Status') }}
                        </th>
                        <th>
                            {{ __('Start time') }}
                        </th>
                        <th>
                            {{ __('End time') }}
                        </th>
                        <th>
                            {{ __('Hashtag') }}
                        </th>
                        <th>
                            {{ __('Comment') }}
                        </th>
                        <th>
                            {{ __('Load') }}
                        </th>
                        <th></th>
                    </tr>
                    @forelse($driverStatuses as $driverStatus)
                        <tr>
                            <td>{{ $driverStatus->id }}</td>
                            <td>{{ $driverStatus->driver ? $driverStatus->driver->name : '-' }}</td>
                            <td>{{ $driverStatus->status_title }}</td>
                            <td>{{ $driverStatus->start_time ? Helper::formatDateTime($driverStatus->start_time) : '-' }}</td>
                            <td>{{ $driverStatus->end_time ? Helper::formatDateTime($driverStatus->end_time) : '-' }}</td>
                            <td>{{ $driverStatus->hashtag }}</td>
                            <td>{{ $driverStatus->comment }}</td>
                            <td>
                                @if ($driverStatus->relatedload)
                                    <a href="{{ route('loads.show', ['load' => $driverStatus->relatedload->id]) }}"
                                        target="_blank" class="btn btn-sm btn-info">{{ __('Load info') }}</a>
                                @endif
                            </td>
                            <td>
                                @can('update-driver-status', $driverStatus)
                                    <a href='{{ route('driver-statuses.edit', [$driverStatus->id]) }}'
                                        class="text-info">{{ __('Edit') }}</a>
                                @endcan
                                @can('delete-driver-status', $driverStatus)
                                    <a href="javascript:;"
                                        onclick="event.preventDefault(); if (confirm('{{ __('Are you sure?') }}')) { $('#delete-driver-status-{{ $driverStatus->id }}').submit() }"
                                        class="text-danger">{{ __('Delete') }}</a>
                                    <form action="{{ route('driver-statuses.destroy', [$driverStatus->id]) }}" method="post"
                                        id="delete-driver-status-{{ $driverStatus->id }}">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8" class="text-center">{{ __('Nothing found') }}</td>
                        </tr>
                    @endforelse
                </table>
            </div>
            {{ $driverStatuses->withQueryString()->links() }}
        </div>


        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
@endsection
