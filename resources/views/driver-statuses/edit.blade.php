@extends('layouts.app')

@section('title', __('Edit driver status'))

@section('content')
    <div class="container">

        <h1>{{ __('Edit driver status') }} - {{ $driverStatus->status_title }}</h1>

        <div class="my-4">
            <a href="{{ route('driver-statuses.index', ['driver_id' => $driverStatus->driver->id]) }}">{{ __('Back') }}</a>
        </div>

        <form action="{{ route('driver-statuses.update', [$driverStatus->id]) }}" method="POST">

            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="form_start_time">{{ __('Start time (east time yyyy-mm-dd hh:mm)') }}</label>
                <input type="text" name="start_time" id="form_start_time" class="form-control datetimepicker-standard"
                    value="{{ old('start_time') ?? ($driverStatus->start_time->format('Y-m-d H:i') ?? '') }}" required>
                @error('start_time')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_end_time">{{ __('End time (east time yyyy-mm-dd hh:mm)') }}</label>
                <input type="text" name="end_time" id="form_end_time" class="form-control datetimepicker-standard"
                    value="{{ old('end_time') ?? (($driverStatus->end_time ? $driverStatus->end_time->format('Y-m-d H:i') : '') ?? '') }}">
                @error('end_time')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <button class="btn btn-lg btn-success" type="submit">{{ __('Save') }}</button>
            </div>
        </form>

    </div>
@endsection
