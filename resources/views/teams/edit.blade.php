@extends('layouts.app')

@section('title', __('Edit team'))

@section('content')
    <div class="container">

        <h1>{{ __('Edit team') }}</h1>

        <div class="py-4">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('teams.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

        <form action="{{ route('teams.update', $team->id) }}" method="POST">

            @csrf
            @method('PUT')

            @include('teams.partials.form')

            <div class="form-group">
                <button class="btn btn-lg btn-success" type="submit">{{ __('Save') }}</button>
            </div>
        </form>

    </div>
@endsection
