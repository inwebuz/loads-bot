@extends('layouts.app')

@section('title', __('Users'))

@section('content')
    <div class="container">

        @include('partials.alerts')

        <div class="my-4">
            <a href="{{ route('users.create') }}" class="btn btn-lg btn-info">
                {{ __('Add user') }}
            </a>
        </div>

        <h1>{{ __('Users') }}</h1>

        <div class="my-4">
            <form action="{{ route('users.index') }}" id="users-filter-form">

                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="form_name">{{ __('Name') }}</label>
                                <input type="text" id="form_name" name="name" class="form-control"
                                    value="{{ $filter['name'] }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="form_email">{{ __('E-mail') }}</label>
                                <input type="text" id="form_email" name="email" class="form-control"
                                    value="{{ $filter['email'] }}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ __('Apply filter') }}</button>
                </div>
            </form>
        </div>

        <div class="my-4">
            <div class="table-responsive">
                <table class="table table-bordered table-light table-hover">
                    <tr class="table-active">
                        <th>
                            {{ __('ID') }}
                        </th>
                        <th>
                            {{ __('E-mail') }}
                        </th>
                        <th>
                            {{ __('Name') }}
                        </th>
                        <th>
                            {{ __('Bot user') }}
                        </th>
                        <th>
                            {{ __('Role') }}
                        </th>
                        <th>
                            {{ __('Active') }}
                        </th>
                        <th></th>
                    </tr>
                    @forelse($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->botUserInfo->full_name ?? '' }}</td>
                            <td>{{ $user->role_name }}</td>
                            <td>{{ $user->active ? __('Yes') : __('No') }}</td>
                            <td class="text-nowrap">
                                <a href="{{ route('users.edit', ['user' => $user->id]) }}"
                                    class="btn btn-sm btn-warning">{{ __('Edit') }}</a>
                                <a href="#" onclick="event.preventDefault(); if (confirm('{{ __('Are you sure?') }}')) { $('#delete-user-{{ $user->id }}').submit() }" class="btn btn-sm btn-danger">{{ __('Delete') }}</a>
                                <form action="{{ route('users.destroy', ['user' => $user->id]) }}" method="post" id="delete-user-{{ $user->id }}">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8" class="text-center">{{ __('Nothing found') }}</td>
                        </tr>
                    @endforelse
                </table>
            </div>
            {{ $users->withQueryString()->links() }}
        </div>


        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
@endsection
