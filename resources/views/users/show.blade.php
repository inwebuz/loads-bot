@extends('layouts.app')

@section('title', __('User') . '#' . $user)

@section('content')
    <div class="container">

    	<h1>
    		{{ __('Driver') }} #{{ $user }}
    	</h1>

        <div class="py-4 d-print-none">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('users.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

    </div>
@endsection
