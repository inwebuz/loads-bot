@extends('layouts.app')

@section('title', __('Drivers board'))

@section('content')
    <div class="container-fluid">

        <h1>
            {{ __('Drivers board') }}
        </h1>

        <div class="my-4">
            <h4>{{ Helper::formatDate($weekStart) }} - {{ Helper::formatDate($weekEnd) }}</h4>
            <form action="{{ route('drivers-board') }}" id="drivers-board-filter-form">

                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_team_id">{{ __('Team') }}</label>
                            <select class="form-control selectpicker" data-live-search="true" name="team_id" id="form_team_id"
                                onchange="document.getElementById('drivers-board-filter-form').submit()">
                                <option value="">{{ __('All') }}</option>
                                @foreach ($teams as $team)
                                    <option value="{{ $team->id }}" @if ((string) $team->id == $filter['team_id']) selected @endif>
                                        {{ $team->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_dispatcher_id">{{ __('Dispatcher') }}</label>
                            <select class="form-control selectpicker" data-live-search="true" name="dispatcher_id"
                                id="form_dispatcher_id"
                                onchange="document.getElementById('drivers-board-filter-form').submit()">
                                <option value="">{{ __('All') }}</option>
                                @foreach ($dispatchersList as $dispatcher)
                                    <option value="{{ $dispatcher->id }}"
                                        @if ((string) $dispatcher->id == $filter['dispatcher_id']) selected @endif>{{ $dispatcher->full_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_year">{{ __('Year') }}</label>
                            <select class="form-control selectpicker" name="year" id="form_year"
                                onchange="document.getElementById('drivers-board-filter-form').submit()">
                                @foreach ($years as $value)
                                    <option value="{{ $value }}" @if ((string) $value == $filter['year']) selected @endif>
                                        {{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_week">{{ __('Week') }}</label>
                            <select class="form-control" name="week" id="form_week"
                                onchange="document.getElementById('drivers-board-filter-form').submit()">
                                @foreach ($weeks as $value)
                                    @php
                                        $date = now()->setISODate($filter['year'], $value);
                                    @endphp
                                    <option value="{{ $value }}" @if ((string) $value == $filter['week']) selected @endif>
                                        {{ $value }} / {{ Helper::formatDate($date->startOfWeek()) }} -
                                        {{ Helper::formatDate($date->endOfWeek()) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="my-4">
            <div class="table-responsive">
                <table class="table table-bordered table-light table-hover">
                    <tr class="table-active">
                        <th>
                            {{ __('ID') }}
                        </th>
                        <th>
                            {{ __('Name') }}
                        </th>
                        <th>
                            {{ __('Main dispatcher') }}
                        </th>

                        <th>
                            {{ __('Weekly target $') }}
                        </th>
                        <th>
                            {{ __('Total $') }}
                        </th>

                        <th>
                            {{ __('Weekly target mile') }}
                        </th>
                        <th>
                            {{ __('Total mile') }}
                        </th>
                        <th>
                            {{ __('Per mile') }}
                        </th>
                    </tr>
                    @forelse($driverPlans as $driverPlan)
                        <tr class="@if($driverPlan->mile < $driverPlan->target_mile) text-danger @endif">
                            <td>{{ $driverPlan->driver->id }}</td>
                            <td>{{ $driverPlan->driver->name }}</td>
                            <td>{{ $driverPlan->driver->dispatcher_name }}</td>

                            <td>
                                {{ Helper::formatPrice($driverPlan->target) }}
                            </td>
                            <td>
                                {{ Helper::formatPrice($driverPlan->total) }}
                            </td>

                            <td>
                                {{ $driverPlan->target_mile }}
                            </td>
                            <td>
                                {{ $driverPlan->mile }}
                            </td>
                            <td>
                                {{ Helper::formatPrice($driverPlan->per_mile) }}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8" class="text-center">{{ __('Nothing found') }}</td>
                        </tr>
                    @endforelse
                </table>
            </div>
        </div>

        {{-- @php
            $teamsList = $teams;
            if ($filter['team_id']) {
                $teamsList = $teamsList->where('id', $filter['team_id']);
            }
        @endphp
        @foreach ($teamsList as $team)
            @php
                $driverPlansList = $driverPlans->where('team_id', $team->id);
            @endphp
            @if ($driverPlansList->isNotEmpty())
                <div class="my-5">
                    <h2 class="bg-dark text-white m-0 p-3" >{{ __('Team') }} {{ $team->name }}</h2>
                    @foreach ($driverPlansList as $driverPlan)
                        @if ($dispatcher->botUser && $dispatcher->botUser->drivers->isNotEmpty())
                            @include('partials.')
                        @endif
                    @endforeach
                </div>
            @endif
        @endforeach --}}


    </div>
@endsection
