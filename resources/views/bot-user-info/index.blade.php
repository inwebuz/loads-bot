@extends('layouts.app')

@section('title', __('Bot users'))

@section('content')
    <div class="container">

        @if (session('success'))
            <div class="alert alert-success">
                {{ __(session('success')) }}
            </div>
        @endif


        <h1>{{ __('Bot users') }}</h1>

        <div class="my-4">
            <div class="table-responsive">
                <table class="table table-light table-bordered table-hover">
                    <tr class="table-active">
                        <th>{{ __('DSP') }}</th>
                        <th>{{ __('QC') }}</th>
                    </tr>
                    <tr>
                        <td>
                            <strong>All: {{ $stats['dsp'] }}</strong> <br>
                            Day: {{ $stats['dsp_day'] }} <br>
                            Night: {{ $stats['dsp_night'] }}
                        </td>
                        <td>
                            <strong> All: {{ $stats['qc'] }}</strong> <br>
                            Day: {{ $stats['qc_day'] }} <br>
                            Night: {{ $stats['qc_night'] }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="my-4">
            <form action="{{ route('bot-user-info.index') }}" id="bot-user-info-filter-form">

                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="form_full_name">{{ __('Name') }}</label>
                                <input type="text" id="form_full_name" name="full_name" class="form-control"
                                    value="{{ $filter['full_name'] }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="form_username">{{ __('Username') }}</label>
                                <input type="text" id="form_username" name="username" class="form-control"
                                    value="{{ $filter['username'] }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_position">{{ __('Position') }}</label>
                            <select class="form-control selectpicker" name="position" id="form_position">
                                <option value="-">All</option>
                                @foreach ($positions as $value)
                                    <option value="{{ $value }}" @if ((string) $value == $filter['position']) selected @endif>
                                        {{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_active">{{ __('Status') }}</label>
                            <select class="form-control selectpicker" name="active" id="form_active">
                                <option value="-">-</option>
                                @foreach ($statuses as $key => $value)
                                    <option value="{{ $key }}" @if ((string) $key == $filter['active']) selected @endif>
                                        {{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ __('Apply filter') }}</button>
                </div>
            </form>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-light table-hover">
                <tr class="table-active">
                    <th>
                        {{ __('Telegram ID') }}
                    </th>
                    <th>
                        {{ __('Full name') }}
                    </th>
                    <th>
                        {{ __('Position') }}
                    </th>
                    <th>
                        {{ __('Team/Shift') }}
                    </th>
                    <th>
                        {{ __('Active') }}
                    </th>
                    <th></th>
                </tr>
                @forelse($botUserInfos as $botUserInfo)
                    <tr>
                        <td>
                            {{ $botUserInfo->user_id }}
                        </td>
                        <td>
                            {{ $botUserInfo->full_name }}
                            @if ($botUserInfo->botUser && $botUserInfo->botUser->username)
                                <a href="https://t.me/{{ $botUserInfo->botUser->username }}"
                                    target="_blank">{{ '@' . $botUserInfo->botUser->username }}</a>
                            @endif
                        </td>
                        <td>
                            <div>
                                <strong>{{ $botUserInfo->position }}</strong> <br>
                                @if ($botUserInfo->dsp_manager)
                                    {{ __('DSP manager') }} <br>
                                @endif
                                @if ($botUserInfo->qc_manager)
                                    {{ __('QC manager') }} <br>
                                @endif
                                @if ($botUserInfo->relations_manager)
                                    {{ __('Relations manager') }} <br>
                                @endif
                                @if ($botUserInfo->trailers_manager)
                                    {{ __('Trailers manager') }} <br>
                                @endif
                                @if ($botUserInfo->trucks_manager)
                                    {{ __('Trucks manager') }} <br>
                                @endif
                            </div>
                        </td>
                        <td>
                            {{ $botUserInfo->shift }} / {{ $botUserInfo->team_name }}
                        </td>
                        <td class="{{ $botUserInfo->active ? 'text-success' : 'text-danger' }}">
                            {{ $botUserInfo->status_title }}
                        </td>
                        <td class="text-nowrap">
                            <a href="{{ route('bot-user-info.edit', [$botUserInfo->id]) }}"
                                class="btn btn-sm btn-warning">{{ __('Edit') }}</a>
                            @if ($botUserInfo->isDsp())
                                <a href="{{ route('kpi-board', ['dispatcher_id' => $botUserInfo->id]) }}"
                                    class="btn btn-sm btn-info" target="_blank">{{ __('KPI board') }}</a>
                            @endif
                            <a href="#"
                                onclick="event.preventDefault(); if (confirm('{{ __('Are you sure?') }}')) { $('#delete-bot-user-info-{{ $botUserInfo->id }}').submit() }"
                                class="btn btn-sm btn-danger">{{ __('Delete') }}</a>
                            <form action="{{ route('bot-user-info.destroy', [$botUserInfo->id]) }}" method="post"
                                id="delete-bot-user-info-{{ $botUserInfo->id }}">
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8" class="text-center">{{ __('Nothing found') }}</td>
                    </tr>
                @endforelse
            </table>
        </div>

        {{ $botUserInfos->withQueryString()->links() }}


        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
@endsection
