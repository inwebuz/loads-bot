@extends('layouts.app')

@section('title', __('Edit load'))

@section('content')
    <div class="container">

        <h1>{{ __('Edit load') }} <a href="{{ route('loads.show', [$load->id]) }}"
                target="_blank">{{ $load->load_and_pcs }}</a></h1>

        <form action="{{ route('loads.update', [$load->id]) }}" method="POST">

            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="form_company_code">{{ __('Company code') }}</label>
                @php
                    $current = old('company_code') ?? ($load->company_code ?? '');
                @endphp
                <select type="text" name="company_code" id="form_company_code" class="selectpicker form-control"
                    data-live-search="true" required>
                    @foreach (\App\Models\Load::companyCodes() as $companyCode)
                        <option value="{{ $companyCode }}" @if ($current == $companyCode) selected @endif>
                            {{ $companyCode }}</option>
                    @endforeach
                </select>
                @error('company_code')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_load_id">{{ __('Load ID') }}</label>
                <input type="text" name="load_id" id="form_load_id" class="form-control"
                    value="{{ old('load_id') ?? ($load->load_id ?? '') }}" required>
                @error('load_id')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_pcs_number">{{ __('PCS number') }}</label>
                <input type="text" name="pcs_number" id="form_pcs_number" class="form-control"
                    value="{{ old('pcs_number') ?? ($load->pcs_number ?? '') }}" required>
                @error('pcs_number')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_rate">{{ __('Rate') }}</label>
                <input type="text" name="rate" id="form_rate" class="form-control"
                    value="{{ old('rate') ?? ($load->rate ?? '') }}" required>
                @error('rate')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_authorized_loaded_mile">{{ __('Authorized loaded mile') }}</label>
                <input type="text" name="authorized_loaded_mile" id="form_authorized_loaded_mile" class="form-control"
                    value="{{ old('authorized_loaded_mile') ?? ($load->authorized_loaded_mile ?? '') }}" required>
                @error('authorized_loaded_mile')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="form_deadhead_mile">{{ __('Load DH mile') }}</label>
                <input type="text" name="deadhead_mile" id="form_deadhead_mile" class="form-control"
                    value="{{ old('deadhead_mile') ?? ($load->deadhead_mile ?? '') }}" required>
                @error('deadhead_mile')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="form_authorized_mile">{{ __('Authorized mile (PLD, TRL, FLT miles)') }}</label>
                <input type="text" name="authorized_mile" id="form_authorized_mile" class="form-control"
                    value="{{ old('authorized_mile') ?? ($load->authorized_mile ?? '') }}" required>
                @error('authorized_mile')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="form_unauthorized_mile">{{ __('Unauthorized miles (including Home DH miles)') }}</label>
                <input type="text" name="unauthorized_mile" id="form_unauthorized_mile" class="form-control"
                    value="{{ old('unauthorized_mile') ?? ($load->unauthorized_mile ?? '') }}" required>
                @error('unauthorized_mile')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_pickup_location">{{ __('PU address (full)') }}</label>
                <input type="text" name="pickup_location" id="form_pickup_location" class="form-control"
                    value="{{ old('pickup_location') ?? ($load->pickup_location ?? '') }}" required>
                @error('pickup_location')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_pickup_timezone">{{ __('PU timezone') }}</label>
                @php
                    $current = old('pickup_timezone') ?? ($load->pickup_timezone ?? '');
                @endphp
                <select type="text" name="pickup_timezone" id="form_pickup_timezone" class="selectpicker form-control"
                    data-live-search="true" required>
                    @foreach (\App\Models\Load::timezones() as $companyCode)
                        <option value="{{ $companyCode }}" @if ($current == $companyCode) selected @endif>
                            {{ $companyCode }}</option>
                    @endforeach
                </select>
                @error('pickup_timezone')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_local_pickup_time">{{ __('PU time (yyyy-mm-dd hh:mm)') }}</label>
                <input type="text" name="local_pickup_time" id="form_local_pickup_time" class="form-control datetimepicker-standard"
                    value="{{ old('local_pickup_time') ?? ($load->local_pickup_time->format('Y-m-d H:i') ?? '') }}" required>
                @error('local_pickup_time')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_delivery_location">{{ __('Delivery address (full)') }}</label>
                <input type="text" name="delivery_location" id="form_delivery_location" class="form-control"
                    value="{{ old('delivery_location') ?? ($load->delivery_location ?? '') }}" required>
                @error('delivery_location')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_delivery_timezone">{{ __('Delivery timezone') }}</label>
                @php
                    $current = old('delivery_timezone') ?? ($load->delivery_timezone ?? '');
                @endphp
                <select type="text" name="delivery_timezone" id="form_delivery_timezone" class="selectpicker form-control"
                    data-live-search="true" required>
                    @foreach (\App\Models\Load::timezones() as $companyCode)
                        <option value="{{ $companyCode }}" @if ($current == $companyCode) selected @endif>
                            {{ $companyCode }}</option>
                    @endforeach
                </select>
                @error('delivery_timezone')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_local_delivery_time">{{ __('Delivery time (yyyy-mm-dd hh:mm)') }}</label>
                <input type="text" name="local_delivery_time" id="form_local_delivery_time"
                    class="form-control datetimepicker-standard"
                    value="{{ old('local_delivery_time') ?? ($load->local_delivery_time->format('Y-m-d H:i') ?? '') }}" required>
                @error('local_delivery_time')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            @if ($load->isThirdParty())
                <div class="form-group">
                    <label for="form_third_company">{{ __('Third company name') }}</label>
                    <input type="text" name="third_company" id="form_third_company" class="form-control"
                        value="{{ old('third_company') ?? ($load->third_company ?? '') }}" required>
                    @error('third_company')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>
            @else
                <div class="form-group">
                    <label for="form_driver_id">{{ __('Driver') }}</label>
                    @php
                        $current = old('driver_id') ?? ($load->driver_id ?? '');
                    @endphp
                    <select type="text" name="driver_id" id="form_driver_id" class="selectpicker form-control"
                        data-live-search="true" required>
                        @foreach ($drivers as $driver)
                            <option value="{{ $driver->id }}" @if ($current == $driver->id) selected @endif>
                                {{ $driver->name }}</option>
                        @endforeach
                    </select>
                    @error('driver_id')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>
            @endif

            <div class="form-group">
                <label for="form_special_note">{{ __('Special note') }}</label>
                <textarea name="special_note" id="form_special_note" class="form-control" required>{{ old('special_note') ?? ($load->special_note ?? '') }}</textarea>
                @error('special_note')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_check_in_as">{{ __('Check in as') }}</label>
                <textarea name="check_in_as" id="form_check_in_as" class="form-control">{{ old('check_in_as') ?? ($load->check_in_as ?? '') }}</textarea>
                @error('check_in_as')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_trailer">{{ __('Trailer') }}</label>
                <textarea name="trailer" id="form_trailer" class="form-control">{{ old('trailer') ?? ($load->trailer ?? '') }}</textarea>
                @error('trailer')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_trailer_note">{{ __('Trailer note') }}</label>
                <textarea name="trailer_note" id="form_trailer_note" class="form-control">{{ old('trailer_note') ?? ($load->trailer_note ?? '') }}</textarea>
                @error('trailer_note')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_pickup_additional_stops">{{ __('Pickup address and time (2, 3, 4 Stops)') }}</label>
                <textarea name="pickup_additional_stops" id="form_pickup_additional_stops" class="form-control">{{ old('pickup_additional_stops') ?? ($load->pickup_additional_stops ?? '') }}</textarea>
                @error('pickup_additional_stops')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            @if ($user->isQc())
                <div class="form-group">
                    <label for="form_pickup_status">{{ __('Pickup status') }}</label>
                    @php
                        $current = old('pickup_status') ?? ($load->pickup_status ?? '');
                    @endphp
                    <select type="text" name="pickup_status" id="form_pickup_status"
                        class="selectpicker form-control" data-live-search="true" required>
                        @foreach (\App\Models\Load::pickupStatuses() as $pickupStatus)
                            <option value="{{ $pickupStatus }}" @if ($current == $pickupStatus) selected @endif>
                                {{ $pickupStatus }}</option>
                        @endforeach
                    </select>
                    @error('pickup_status')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="form_pickup_qc_comment">{{ __('Pickup QC comment') }}</label>
                    @php
                        $current = old('pickup_qc_comment') ?? ($load->pickup_qc_comment ?? '-');
                    @endphp
                    <select type="text" name="pickup_qc_comment" id="form_pickup_qc_comment"
                        class="selectpicker form-control" data-live-search="true" required>
                        @foreach (\App\Models\Load::pickupQcComments() as $pickupQcComment)
                            <option value="{{ $pickupQcComment }}" @if ($current == $pickupQcComment) selected @endif>
                                {{ $pickupQcComment }}</option>
                        @endforeach
                    </select>
                    @error('pickup_qc_comment')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="form_delivery_status">{{ __('Delivery status') }}</label>
                    @php
                        $current = old('delivery_status') ?? ($load->delivery_status ?? '');
                    @endphp
                    <select type="text" name="delivery_status" id="form_delivery_status"
                        class="selectpicker form-control" data-live-search="true" required>
                        @foreach (\App\Models\Load::deliveryStatuses() as $deliveryStatus)
                            <option value="{{ $deliveryStatus }}" @if ($current == $deliveryStatus) selected @endif>
                                {{ $deliveryStatus }}</option>
                        @endforeach
                    </select>
                    @error('delivery_status')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="form_delivery_qc_comment">{{ __('Delivery QC comment') }}</label>
                    @php
                        $current = old('delivery_qc_comment') ?? ($load->delivery_qc_comment ?? '-');
                    @endphp
                    <select type="text" name="delivery_qc_comment" id="form_delivery_qc_comment"
                        class="selectpicker form-control" data-live-search="true" required>
                        @foreach (\App\Models\Load::deliveryQcComments() as $deliveryQcComment)
                            <option value="{{ $deliveryQcComment }}" @if ($current == $deliveryQcComment) selected @endif>
                                {{ $deliveryQcComment }}</option>
                        @endforeach
                    </select>
                    @error('delivery_qc_comment')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>
            @endif

            <div class="form-group">
                <button class="btn btn-lg btn-success" type="submit">{{ __('Save') }}</button>
            </div>
        </form>

    </div>
@endsection
