@extends('layouts.app')

@section('title', __('Load') . '#' . $load->load_id)

@section('content')
    <div class="container">

        @include('partials.alerts')

        <h1>
            {{ __('Load') }} {{ $load->load_id }}
        </h1>

        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td>
                        <strong>{{ __('Company code') }}</strong>
                    </td>
                    <td>
                        {{ $load->company_code }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Load ID') }}</strong>
                    </td>
                    <td>
                        {{ $load->load_id }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Type') }}</strong>
                    </td>
                    <td>
                        {{ $load->type_title }}
                    </td>
                </tr>
                {{-- <tr>
                    <td>
                        <strong>{{ __('Status') }}</strong>
                    </td>
                    <td>
                        {{ $load->status_title }}
                    </td>
                </tr> --}}
                <tr>
                    <td>
                        <strong>{{ __('Driver') }}</strong>
                    </td>
                    <td>
                        {{ $load->driver_name }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Third company') }}</strong>
                    </td>
                    <td>
                        {{ $load->third_company }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('PCS number') }}</strong>
                    </td>
                    <td>
                        {{ $load->pcs_number }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Pickup status') }}</strong>
                    </td>
                    <td>
                        {{ $load->pickup_status }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Pickup QC comment') }}</strong>
                    </td>
                    <td>
                        {{ $load->pickup_qc_comment }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Delivery status') }}</strong>
                    </td>
                    <td>
                        {{ $load->delivery_status }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Delivery QC comment') }}</strong>
                    </td>
                    <td>
                        {{ $load->delivery_qc_comment }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Rate') }}</strong>
                    </td>
                    <td>
                        {{ $load->rate }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Mile') }}</strong>
                    </td>
                    <td>
                        {{ __('Authorized loaded mile') }}: {{ $load->authorized_loaded_mile }} <br>
                        {{ __('Load DH mile') }}: {{ $load->deadhead_mile }} <br>
                        {{ __('Authorized mile (PLD, TRL, FLT miles)') }}: {{ $load->authorized_mile }} <br>
                        {{ __('Unauthorized miles (including Home DH miles)') }}: {{ $load->unauthorized_mile }} <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('PU address (full)') }}</strong>
                    </td>
                    <td>
                        {{ $load->pickup_location }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('PU time') }}</strong>
                    </td>
                    <td>
                        {{ $load->pickup_time ? Helper::formatDateTime($load->pickup_time) : '-' }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Delivery address (full)') }}</strong>
                    </td>
                    <td>
                        {{ $load->delivery_location }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Delivery time') }}</strong>
                    </td>
                    <td>
                        {{ $load->delivery_time ? Helper::formatDateTime($load->delivery_time) : '-' }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Special note') }}</strong>
                    </td>
                    <td>
                        {{ $load->special_note }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Check in as') }}</strong>
                    </td>
                    <td>
                        {{ $load->check_in_as }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Trailer') }}</strong>
                    </td>
                    <td>
                        {{ $load->trailer }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Trailer note') }}</strong>
                    </td>
                    <td>
                        {{ $load->trailer_note }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Pickup address and time (2, 3, 4 Stops)') }}</strong>
                    </td>
                    <td>
                        {{ $load->pickup_additional_stops }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('DSP') }}</strong>
                    </td>
                    <td>
                        {{ $load->dsp_full_name }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('DSP bonus') }}</strong>
                    </td>
                    <td>
                        {{ $load->dsp_bonus }} %
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('QC') }}</strong>
                    </td>
                    <td>
                        {{ $load->qc_full_name }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Created') }}</strong>
                    </td>
                    <td>
                        {{ Helper::formatDateTime($load->created_at) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('Updated') }}</strong>
                    </td>
                    <td>
                        {{ Helper::formatDateTime($load->updated_at) }}
                    </td>
                </tr>
                @if ($load->isCancelled())
                    <tr>
                        <td>
                            <strong>{{ __('Status') }}</strong>
                        </td>
                        <td>
                            <div class="text-danger">{{ $load->status_title }}</div>
                            <div>{{ __('TONU') }}: {{ $load->tonu }}</div>
                            <div>{{ __('Comment') }}: {{ $load->cancel_comment }}</div>
                        </td>
                    </tr>
                @endif

                <tr class="d-print-none">
                    <td>
                        <strong>&nbsp;</strong>
                    </td>
                    <td>
                        @can('update-load', $load)
                            <a href='{{ route('loads.edit', [$load->id]) }}' class='text-info'
                                target='_blank'>{{ __('Edit') }}</a>
                        @endcan
                        @can('cancel-load', $load)
                            <a href='{{ route('loads.cancel', [$load->id]) }}' class='text-danger'
                                target='_blank'>{{ __('Cancel') }}</a>
                        @endcan
                        @can('delete-load', $load)
                            <a href="#"
                                onclick="event.preventDefault(); if (confirm('{{ __('Are you sure?') }}')) { $('#delete-load-{{ $load->id }}').submit() }"
                                class="text-danger">{{ __('Delete') }}</a>
                            <form action="{{ route('loads.destroy', ['load' => $load->id]) }}" method="post"
                                id="delete-load-{{ $load->id }}">
                                @csrf
                                @method('DELETE')
                            </form>
                        @endcan
                    </td>
                </tr>
            </table>
        </div>

        <div class="py-4 d-print-none">
            <a href="{{ url()->previous() && url()->previous() != url()->current() ? url()->previous() : route('loads.index') }}"
                class="btn btn-info">{{ __('Back') }}</a>
        </div>

    </div>
@endsection
