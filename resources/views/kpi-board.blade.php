@extends('layouts.app')

@section('title', __('KPI board'))

@section('content')
    <div class="container-fluid">

        <h1>
            {{ __('KPI board') }}
        </h1>

        <div class="my-4 position-relative">
            <div class="d-flex justify-content-between">
                <h4>{{ Helper::formatDate($weekStart) }} - {{ Helper::formatDate($weekEnd) }}</h4>
                <div>
                    <button class="btn btn-success" type="submit" name="download" value="download"
                        form="kpi-board-filter-form">{{ __('Download') }}</button>
                </div>
            </div>
            <form action="{{ route('kpi-board') }}" id="kpi-board-filter-form">
                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_team_id">{{ __('Team') }}</label>
                            <select class="form-control selectpicker" data-live-search="true" name="team_id"
                                id="form_team_id" onchange="document.getElementById('kpi-board-filter-form').submit()">
                                <option value="">{{ __('All') }}</option>
                                @foreach ($teams as $team)
                                    <option value="{{ $team->id }}" @if ((string) $team->id == $filter['team_id']) selected @endif>
                                        {{ $team->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_dispatcher_id">{{ __('Dispatcher') }}</label>
                            <select class="form-control selectpicker" data-live-search="true" name="dispatcher_id"
                                id="form_dispatcher_id"
                                onchange="document.getElementById('kpi-board-filter-form').submit()">
                                <option value="">{{ __('All') }}</option>
                                @foreach ($dispatchersList as $dispatcher)
                                    <option value="{{ $dispatcher->id }}"
                                        @if ((string) $dispatcher->id == $filter['dispatcher_id']) selected @endif>{{ $dispatcher->full_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{-- <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_shift">{{ __('Shift') }}</label>
                            <select class="form-control selectpicker" data-live-search="true" name="shift"
                                id="form_shift"
                                onchange="document.getElementById('kpi-board-filter-form').submit()">
                                <option value="">{{ __('All') }}</option>
                                @foreach ($shifts as $shift)
                                    <option value="{{ $shift }}"
                                        @if ($shift == $filter['shift']) selected @endif>{{ $shift }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div> --}}
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_year">{{ __('Year') }}</label>
                            <select class="form-control selectpicker" name="year" id="form_year"
                                onchange="document.getElementById('kpi-board-filter-form').submit()">
                                @foreach ($years as $value)
                                    <option value="{{ $value }}" @if ((string) $value == $filter['year']) selected @endif>
                                        {{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_week">{{ __('Week') }}</label>
                            <select class="form-control" name="week" id="form_week"
                                onchange="document.getElementById('kpi-board-filter-form').submit()">
                                @foreach ($weeks as $value)
                                    @php
                                        $date = now()->setISODate($filter['year'], $value);
                                    @endphp
                                    <option value="{{ $value }}" @if ((string) $value == $filter['week']) selected @endif>
                                        {{ $value }} / {{ Helper::formatDate($date->startOfWeek()) }} -
                                        {{ Helper::formatDate($date->endOfWeek()) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        @php
            $teamsList = $teams;
            if ($filter['team_id']) {
                $teamsList = $teamsList->where('id', $filter['team_id']);
            }
        @endphp
        @foreach ($teamsList as $team)
            @php
                $dispatchersList = $dispatchers->where('team_id', $team->id);
            @endphp
            @if ($dispatchersList->isNotEmpty())
                <div class="my-5">
                    <h2 class="bg-dark text-white m-0 p-3">{{ __('Team') }} {{ $team->name }}</h2>
                    @foreach ($dispatchersList as $dispatcher)
                        @if ($dispatcher->botUser && $dispatcher->botUser->drivers->isNotEmpty())
                            @include('partials.dispatcher-kpi')
                        @endif
                    @endforeach
                </div>
            @endif
        @endforeach


    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.4.0/chart.umd.js"
        integrity="sha512-6HrPqAvK+lZElIZ4mZ64fyxIBTsaX5zAFZg2V/2WT+iKPrFzTzvx6QAsLW2OaLwobhMYBog/+bvmIEEGXi0p1w=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $(function() {
            $('.driver-statuses-pie-chart').each(function() {
                let elem = $(this)
                let driverId = $(this).data('id')
                let lengthAll = 0
                let statusValues = {}
                let inputs = $('.driver-status-input[data-driver-id="' + driverId + '"]')
                inputs.each(function() {
                    let status = $(this).data('status')
                    if (!statusValues[status]) {
                        statusValues[status] = {
                            length: 0,
                            color: '#000000'
                        }
                    }
                    lengthAll += +$(this).data('length')
                    statusValues[status]['length'] += +$(this).data('length')
                    statusValues[status]['color'] = $(this).data('color')
                })
                if (!statusValues) {
                    return
                }
                let chartLabels = []
                let chartColors = []
                let chartData = []
                for (let i in statusValues) {
                    chartLabels.push(i)
                    chartColors.push(statusValues[i]['color'])
                    let percent = (statusValues[i]['length'] / lengthAll * 100).toFixed(2)
                    chartData.push(percent)
                }

                let data = {
                    labels: chartLabels,
                    datasets: [{
                        label: '-',
                        data: chartData,
                        backgroundColor: chartColors,
                    }]
                };
                let config = {
                    type: 'pie',
                    data: data,
                    options: {
                        responsive: true,
                        aspectRatio: 2,
                        plugins: {
                            legend: {
                                position: 'right',
                            },
                            tooltip: {
                                callbacks: {
                                    label: (context) => ' ' + context.formattedValue + '%'
                                }
                            }
                        }
                    },
                };

                let chart = new Chart(elem, config);

            })
        })
    </script>
@endsection
