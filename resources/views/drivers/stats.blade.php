@extends('layouts.app')

@section('title', __('Driver') . ' ' . $driver->name . ' ' . __('Stats'))

@section('content')
    <div class="container-fluid">

    	<h1>
    		{{ __('Driver') }} {{ $driver->name }} {{ __('Stats') }}
    	</h1>

        <div class="py-4 d-print-none">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('drivers.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

        <div class="my-4">
            <div class="mb-1">
                <strong>{{ __('Weekly target') }}</strong>: {{ Helper::formatPrice($stats['weeklyTarget']) }}
            </div>
            <div class="mb-1 text-{{ $stats['completionStatus'] }} @if(in_array($stats['completionStatus'], ['warning'])) bg-dark @endif">
                <strong>{{ __('Completed') }}</strong>: {{ Helper::formatPrice($stats['loadsTotal']) }} | {{ $stats['completedPercent'] }}%
            </div>
            <div class="text-{{ $stats['perMileStatus'] }} @if(in_array($stats['perMileStatus'], ['warning'])) bg-dark @endif">
                <strong>{{ __('Per mile') }}</strong>: {{ Helper::formatPrice($stats['perMile']) }}
            </div>
        </div>

        <div class="my-4">
            <h4>{{ Helper::formatDate($weekStart) }} - {{ Helper::formatDate($weekEnd) }}</h4>
            <form action="{{ route('drivers.stats', [$driver->id]) }}" id="drivers-stats-filter-form">

                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_year">{{ __('Year') }}</label>
                            <select class="form-control" name="year" id="form_year" onchange="document.getElementById('drivers-stats-filter-form').submit()">
                                @foreach ($years as $value)
                                    <option value="{{ $value }}" @if ((string) $value == $filter['year']) selected @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_week">{{ __('Week') }}</label>
                            <select class="form-control" name="week" id="form_week" onchange="document.getElementById('drivers-stats-filter-form').submit()">
                                @foreach ($weeks as $value)
                                    <option value="{{ $value }}" @if ((string) $value == $filter['week']) selected @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="my-4">
            <h4>{{ __('Loads') }}</h4>
            <div class="table-responsive">
                <table class="table table-bordered table-light table-hover">
                    <tr class="table-active">
                        <th>
                            {{ __('Load ID') }}
                        </th>
                        <th>
                            {{ __('Type') }}
                        </th>
                        <th>
                            {{ __('PCS number') }}
                        </th>
                        <th>
                            {{ __('Pickup / Delivery') }}
                        </th>

                        <th>
                            {{ __('Total $') }}
                        </th>
                        <th>
                            {{ __('Total mile') }}
                        </th>

                        <th>
                            {{ __('DSP') }}
                        </th>
                        <th></th>
                    </tr>
                    @forelse($stats['loads'] as $load)
                        <tr>
                            <td class="text-center">
                                <strong>{{ $load->load_id }}</strong>
                            </td>
                            <td>
                                {{ $load->type_title }}
                            </td>
                            <td>
                                {{ $load->pcs_number }}
                            </td>
                            <td>
                                <div>
                                    PU: {{ $load->pickup_location }} {{ $load->pickup_time }}
                                </div>
                                <div>
                                    Del: {{ $load->delivery_location }} {{ $load->delivery_time }}
                                </div>
                            </td>

                            <td>
                                {{ $load->total }}
                            </td>
                            <td>
                                {{ $load->mile }}
                            </td>

                            <td>
                                {{ $load->dsp_full_name }}
                            </td>
                            <td class="text-nowrap">
                                <a href="{{ route('loads.show', ['load' => $load->id]) }}" class="btn btn-sm btn-info" target="_blank">{{ __('View more') }}</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="10" class="text-center">{{ __('Nothing found') }}</td>
                        </tr>
                    @endforelse
                </table>
            </div>
        </div>

    </div>
@endsection
