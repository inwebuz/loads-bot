@extends('layouts.app')

@section('title', __('Driver') . '#' . $driver)

@section('content')
    <div class="container">

    	<h1>
    		{{ __('Driver') }} #{{ $driver }}
    	</h1>

        <div class="py-4 d-print-none">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('drivers.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

    </div>
@endsection
