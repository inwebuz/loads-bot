

@if (isset($formType) && $formType == 'edit')
<input type="hidden" name="key" value="{{ $setting->key }}">
@else
<div class="form-group">
    <label for="form_key">{{ __('Key') }}</label>
    <input type="text" name="key" id="form_key" class="form-control" value="{{ old('key') ?? $setting->key }}" required>
    @error('key')
    <div class="invalid-feedback d-block">{{ $description }}</div>
    @enderror
</div>
@endif

<div class="form-group">
    <label for="form_name">{{ __('Title') }}</label>
    <input type="text" name="name" id="form_name" class="form-control" value="{{ old('name') ?? $setting->name }}" required>
    @error('name')
    <div class="invalid-feedback d-block">{{ $description }}</div>
    @enderror
</div>


<div class="form-group">
    <label for="form_description">{{ __('Description') }}</label>
    <textarea type="text" name="description" id="form_description" rows="6" class="form-control" required>{{ old('description') ?? $setting->description }}</textarea>
    @error('description')
    <div class="invalid-feedback d-block">{{ $description }}</div>
    @enderror
</div>

