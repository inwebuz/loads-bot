@extends('layouts.app')

@section('title', __('Team sheet'))

@section('content')
    <div class="container-fluid">

        <h1>
            {{ __('Team sheet') }}
        </h1>

        <div class="my-4">
            <form action="{{ route('team-sheet') }}" id="main-team-sheet-filter-form">
                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_team_id">{{ __('Team') }}</label>
                            <select class="form-control selectpicker" data-live-search="true" name="team_id" id="form_team_id"
                                onchange="document.getElementById('main-team-sheet-filter-form').submit()">
                                <option value="">{{ __('All') }}</option>
                                @foreach ($teams as $team)
                                    <option value="{{ $team->id }}" @if ((string) $team->id == $filter['team_id']) selected @endif>
                                        {{ $team->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_dispatcher_id">{{ __('Dispatcher') }}</label>
                            <select class="form-control selectpicker" data-live-search="true" name="dispatcher_id"
                                id="form_dispatcher_id"
                                onchange="document.getElementById('main-team-sheet-filter-form').submit()">
                                <option value="">{{ __('All') }}</option>
                                @foreach ($dispatchersList as $dispatcher)
                                    <option value="{{ $dispatcher->id }}" @if ((string) $dispatcher->id == $filter['dispatcher_id']) selected @endif>
                                        {{ $dispatcher->full_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        @php
            $teamsList = $teams;
            if ($filter['team_id']) {
                $teamsList = $teamsList->where('id', $filter['team_id']);
            }
        @endphp
        <div class="mb-5 position-relative">
            <table class="table table-borderless table-hover">
                <tr class="bg-white">
                    <th>#</th>
                    <th>{{ __('Load') }}</th>
                    <th>{{ __('Driver') }}</th>
                    <th>{{ __('Truck') }}</th>
                    <th>{{ __('Phone number') }}</th>
                    <th>{{ __('Status') }}</th>
                    <th>{{ __('Pick up') }}</th>
                    <th>{{ __('Delivery') }}</th>
                    <th>{{ __('Trailer') }}</th>
                    <th>{{ __('PM') }}</th>
                </tr>
                @foreach ($teamsList as $team)
                    @php
                        $dispatchersList = $dispatchers->where('team_id', $team->id);
                    @endphp
                    @if ($dispatchersList->isNotEmpty())
                        <tr class="bg-dark text-white">
                            <td colspan="10" class="p-3">
                                <h4 class="mb-0">{{ __('Team') }} {{ $team->name }}</h4>
                            </td>
                        </tr>
                        @foreach ($dispatchersList as $dispatcher)
                            @if ($dispatcher->botUser && $dispatcher->botUser->drivers->isNotEmpty())
                                <tr class="bg-primary text-white position-sticky top-0">
                                    <td colspan="10" class="p-2">
                                        <h5 class="mb-1">{{ $dispatcher->full_name }}</h5>
                                        <div id="dsp-{{ $dispatcher->id }}-board-info"
                                            style="white-space: nowrap;" data-table="bot_user_info" data-field="teamsheet_info" data-id="{{ $dispatcher->id }}">
                                            {{ $dispatcher->teamsheet_info }} &nbsp;
                                        </div>
                                        <a href="javascript:;" class="edit-table-info-btn text-white"
                                            data-target="#dsp-{{ $dispatcher->id }}-board-info">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                                @foreach ($dispatcher->botUser->drivers as $key => $driver)
                                    @php
                                        $load = $driver->relatedload;
                                        $trailer = $driver->trailer;
                                    @endphp
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>
                                            @if ($load)
                                                <a href="javascript:;" data-toggle="popover" data-trigger="focus"
                                                    title="Load ID: {{ $load->load_id }}" data-html="true"
                                                    data-content="@include('partials.load-popover-content')" class="text-dark">
                                                    {{ $load->load_and_pcs }}
                                                    <i class="fa-regular fa-eye"></i>
                                                </a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if (auth()->user()->isAdmin())
                                                <a href="{{ route('drivers.index', ['id' => $driver->id]) }}"
                                                    target="_blank" class="text-dark">
                                                    {{ $driver->name_and_id }}
                                                    <i class="fa-solid fa-link"></i>
                                                </a>
                                            @else
                                                {{ $driver->name }}
                                            @endif
                                        </td>
                                        <td>
                                            {{ $driver->truck_number }}
                                        </td>
                                        <td>
                                            {{ $driver->phone_number }}
                                        </td>
                                        <td style="background-color: {{ $driver->status_color }};"
                                            class="text-white text-center align-middle">
                                            <a class="text-white" href="javascript:;" data-toggle="popover"
                                                data-trigger="focus" title="Status: {{ $driver->status_title }}"
                                                data-html="true" data-content="@include('partials.driver-status-popover-content', [
                                                    'driverStatus' => $driver->currentDriverStatus,
                                                ])">
                                                {{ $driver->status_title }}
                                            </a>
                                        </td>
                                        <td>
                                            @if ($load)
                                                <div class="mb-1">
                                                    PU: {{ $load->pickup_location }} <br>
                                                    <i>{{ $load->pickup_time }}</i>
                                                </div>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if ($load)
                                                <div>
                                                    Del: {{ $load->delivery_location }} <br>
                                                    <i>{{ $load->delivery_time }}</i>
                                                </div>
                                            @endif
                                            @if ($driver->currentDriverStatus && $driver->currentDriverStatus->isHome())
                                                <div>
                                                    Home address: {{ $driver->home_address }}
                                                </div>
                                            @endif
                                            @if ($driver->currentDriverStatus && $driver->currentDriverStatus->shop_address)
                                                <div>
                                                    Shop address: {{ $driver->currentDriverStatus->shop_address }}
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            {{-- @if ($trailer)
                                                    {{ $trailer->trailer_number }}
                                                @else
                                                    -
                                                @endif --}}
                                            <div id="drivers-{{ $driver->id }}-trailer-info" data-table="drivers"
                                                data-field="trailer_info" data-id="{{ $driver->id }}">
                                                {{ $driver->trailer_info }}
                                            </div>
                                            <a href="javascript:;" class="edit-table-info-btn"
                                                data-target="#drivers-{{ $driver->id }}-trailer-info">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <div id="drivers-{{ $driver->id }}-pm-info" data-table="drivers" data-field="pm_info" data-field="pm_info" data-id="{{ $driver->id }}">
                                                {{ $driver->pm_info }}
                                            </div>
                                            <a href="javascript:;" class="edit-table-info-btn" data-target="#drivers-{{ $driver->id }}-pm-info">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </table>
        </div>
    </div>
@endsection
