@extends('layouts.app')

@section('title', __('Edit trailer'))

@section('content')
    <div class="container">

        <h1>{{ __('Edit trailer') }}</h1>

        <div class="py-4">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('trailers.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

        <form action="{{ route('trailers.update', $trailer->id) }}" method="POST">

            @csrf
            @method('PUT')

            @include('trailers.partials.form')

            <div class="form-group">
                <button class="btn btn-lg btn-success" type="submit">{{ __('Save') }}</button>
            </div>
        </form>

    </div>
@endsection
