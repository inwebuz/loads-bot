<div class="form-group">
    <label for="form_trailer_number">{{ __('Trailer number') }}</label>
    <input type="text" name="trailer_number" id="form_trailer_number" class="form-control" value="{{ old('trailer_number') ?? $trailer->trailer_number }}" required>
    @error('trailer_number')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>
