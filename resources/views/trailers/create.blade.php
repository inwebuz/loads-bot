@extends('layouts.app')

@section('title', __('Add trailer'))

@section('content')
    <div class="container">

        <h1>{{ __('Add trailer') }}</h1>

        <div class="py-4">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('trailers.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

        <form action="{{ route('trailers.store') }}" method="POST">

            @csrf

            @include('trailers.partials.form')

            <div class="form-group">
                <button class="btn btn-lg btn-success" type="submit">{{ __('Save') }}</button>
            </div>
        </form>

    </div>
@endsection
