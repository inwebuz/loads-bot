@extends('layouts.app')

@section('title', __('Stats') . ' - ' . __('Drivers'))

@section('content')
    <div class="container-fluid">

    	<h1>
    		{{ __('Stats') }} - {{ __('Drivers') }}
    	</h1>

        <div class="my-4">
            <h4>{{ Helper::formatDate($weekStart) }} - {{ Helper::formatDate($weekEnd) }}</h4>
            <form action="{{ route('stats.drivers') }}" id="stats-drivers-filter-form">

                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_year">{{ __('Year') }}</label>
                            <select class="form-control" name="year" id="form_year" onchange="document.getElementById('stats-drivers-filter-form').submit()">
                                @foreach ($years as $value)
                                    <option value="{{ $value }}" @if ((string) $value == $filter['year']) selected @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_week">{{ __('Week') }}</label>
                            <select class="form-control" name="week" id="form_week" onchange="document.getElementById('stats-drivers-filter-form').submit()">
                                @foreach ($weeks as $value)
                                    <option value="{{ $value }}" @if ((string) $value == $filter['week']) selected @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="my-4">
            <div class="table-responsive">
                <table class="table table-bordered table-light table-hover">
                    <tr class="table-active">
                        <th>
                            {{ __('ID') }}
                        </th>
                        <th>
                            {{ __('Name') }}
                        </th>
                        <th>
                            {{ __('Main dispatcher') }}
                        </th>
                        <th>
                            {{ __('Stats') }}
                        </th>
                        <th></th>
                    </tr>
                    @forelse($drivers as $driver)
                        <tr>
                            <td>{{ $driver->id }}</td>
                            <td>{{ $driver->name }}</td>
                            <td>{{ $driver->dispatcher_name }}</td>
                            <td>
                                @php
                                    $stats = Helper::getDriverWeeklyStats($driver, $weekStart, $weekEnd);
                                @endphp
                                <div class="mb-1">
                                    <strong>{{ __('Weekly target') }}</strong>: {{ Helper::formatPrice($stats['weeklyTarget']) }}
                                </div>
                                <div class="mb-1 text-{{ $stats['completionStatus'] }} @if(in_array($stats['completionStatus'], ['warning'])) bg-dark @endif">
                                    <strong>{{ __('Completed') }}</strong>: {{ Helper::formatPrice($stats['loadsTotal']) }} | {{ $stats['completedPercent'] }}%
                                </div>
                                <div class="text-{{ $stats['perMileStatus'] }} @if(in_array($stats['perMileStatus'], ['warning'])) bg-dark @endif">
                                    <strong>{{ __('Per mile') }}</strong>: {{ Helper::formatPrice($stats['perMile']) }}
                                </div>
                            </td>
                            <td class="text-nowrap">
                                <a href="{{ route('drivers.stats', ['driver' => $driver->id]) }}" class="btn btn-sm btn-primary" target="_blank">{{ __('Stats') }}</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8" class="text-center">{{ __('Nothing found') }}</td>
                        </tr>
                    @endforelse
                </table>
            </div>
            {{ $drivers->withQueryString()->links() }}
        </div>

    </div>
@endsection
