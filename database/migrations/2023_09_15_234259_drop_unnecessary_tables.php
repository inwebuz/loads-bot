<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropUnnecessaryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ad_distribution_items');
        Schema::dropIfExists('ad_distributions');
        Schema::dropIfExists('ads');
        Schema::dropIfExists('banner_stats');
        Schema::dropIfExists('banners');
        Schema::dropIfExists('cart_items');
        Schema::dropIfExists('carts');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('click_payments');
        Schema::dropIfExists('paycom_transactions');
        Schema::dropIfExists('contacts');
        Schema::dropIfExists('dayoffs');
        Schema::dropIfExists('galleries');
        Schema::dropIfExists('images');
        Schema::dropIfExists('items');
        Schema::dropIfExists('links');
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('pages');
        Schema::dropIfExists('products');
        Schema::dropIfExists('publications');
        Schema::dropIfExists('redirects');
        Schema::dropIfExists('reviews');
        Schema::dropIfExists('rubrics');
        Schema::dropIfExists('sellers');
        Schema::dropIfExists('sms_verifications');
        Schema::dropIfExists('static_texts');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
