<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeLoadQcFieldsInLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loads', function (Blueprint $table) {
            $table->renameColumn('pickup_ontime', 'pickup_status');
            $table->renameColumn('pickup_ontime_comment', 'pickup_qc_comment');
            $table->renameColumn('delivery_ontime', 'delivery_status');
            $table->renameColumn('delivery_ontime_comment', 'delivery_qc_comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loads', function (Blueprint $table) {
            $table->renameColumn('pickup_status', 'pickup_ontime');
            $table->renameColumn('pickup_qc_comment', 'pickup_ontime_comment');
            $table->renameColumn('delivery_status', 'delivery_ontime');
            $table->renameColumn('delivery_qc_comment', 'delivery_ontime_comment');
        });
    }
}
