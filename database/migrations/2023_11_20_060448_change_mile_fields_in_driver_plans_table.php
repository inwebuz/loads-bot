<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeMileFieldsInDriverPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('driver_plans', function (Blueprint $table) {
            $table->decimal('authorized_loaded_mile', 12, 2)->default(0)->after('total_mile');
            $table->decimal('authorized_mile', 12, 2)->default(0)->after('total_deadhead_mile');
            $table->decimal('unauthorized_mile', 12, 2)->default(0)->after('authorized_mile');
            $table->renameColumn('total_mile', 'mile');
            $table->renameColumn('total_deadhead_mile', 'deadhead_mile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_plans', function (Blueprint $table) {
            $table->dropColumn(['authorized_loaded_mile', 'authorized_mile', 'unauthorized_mile']);
            $table->renameColumn('mile', 'total_mile');
            $table->renameColumn('deadhead_mile', 'total_deadhead_mile');
        });
    }
}
