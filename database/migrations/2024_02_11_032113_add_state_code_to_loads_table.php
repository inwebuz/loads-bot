<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStateCodeToLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loads', function (Blueprint $table) {
            $table->string('pickup_state_code', 3)->nullable();
            $table->string('pickup_zip_code', 10)->nullable();
            $table->string('delivery_state_code', 3)->nullable();
            $table->string('delivery_zip_code', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loads', function (Blueprint $table) {
            $table->dropColumn(['pickup_state_code', 'pickup_zip_code', 'delivery_state_code', 'delivery_zip_code']);
        });
    }
}
