<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDspCommentToDriverPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('driver_plans', function (Blueprint $table) {
            $table->text('dsp_comment')->nullable();
            $table->text('manager_comment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_plans', function (Blueprint $table) {
            $table->dropColumn(['dsp_comment', 'manager_comment']);
        });
    }
}
