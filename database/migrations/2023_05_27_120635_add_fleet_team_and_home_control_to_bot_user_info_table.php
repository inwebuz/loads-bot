<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFleetTeamAndHomeControlToBotUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bot_user_info', function (Blueprint $table) {
            $table->tinyInteger('is_fleet_team_member')->default(0);
            $table->tinyInteger('is_home_control_team_member')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bot_user_info', function (Blueprint $table) {
            $table->dropColumn(['is_fleet_team_member', 'is_home_control_team_member']);
        });
    }
}
