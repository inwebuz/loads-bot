<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddManagerFieldsToBotUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bot_user_info', function (Blueprint $table) {
            $table->tinyInteger('dsp_manager')->default(0);
            $table->tinyInteger('qc_manager')->default(0);
            $table->tinyInteger('relations_manager')->default(0);
            $table->tinyInteger('trailers_manager')->default(0);
            $table->tinyInteger('trucks_manager')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bot_user_info', function (Blueprint $table) {
            $table->dropColumn(['dsp_manager', 'qc_manager', 'relations_manager', 'trailers_manager', 'trucks_manager']);
        });
    }
}
