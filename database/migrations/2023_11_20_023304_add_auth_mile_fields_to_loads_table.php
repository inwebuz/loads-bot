<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAuthMileFieldsToLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loads', function (Blueprint $table) {
            $table->decimal('authorized_loaded_mile', 12, 2)->default(0)->after('cancel_comment');
            $table->decimal('authorized_mile', 12, 2)->default(0)->after('deadhead_mile');
            $table->decimal('unauthorized_mile', 12, 2)->default(0)->after('authorized_mile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loads', function (Blueprint $table) {
            $table->dropColumn(['authorized_loaded_mile', 'authorized_mile', 'unauthorized_mile']);
        });
    }
}
