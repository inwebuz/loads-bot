<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStateCodeToDriverStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('driver_statuses', function (Blueprint $table) {
            $table->string('shop_address_state_code', 3)->nullable();
            $table->string('shop_address_zip_code', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_statuses', function (Blueprint $table) {
            $table->dropColumn(['shop_address_state_code', 'shop_address_zip_code']);
        });
    }
}
