<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimezoneToLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loads', function (Blueprint $table) {
            $table->string('pickup_timezone', 3)->nullable()->after('pickup_location');
            $table->timestamp('local_pickup_time')->nullable()->after('pickup_time');
            $table->string('delivery_timezone', 3)->nullable()->after('delivery_location');
            $table->timestamp('local_delivery_time')->nullable()->after('delivery_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loads', function (Blueprint $table) {
            $table->dropColumn(['pickup_timezone', 'local_pickup_time', 'delivery_timezone', 'local_delivery_time']);
        });
    }
}
