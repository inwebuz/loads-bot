<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class ManagersGroupSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::updateOrCreate(
            [
                'key' => 'managers_group',
            ],
            [
                'name' => 'Managers group ID (/managerschat)',
                'description' => '',
                'visibility' => 1,
            ]
        );
        Setting::updateOrCreate(
            [
                'key' => 'fleetteam_group',
            ],
            [
                'name' => 'Fleet team group ID (/fleetteamchat)',
                'description' => '',
                'visibility' => 1,
            ]
        );
    }
}
