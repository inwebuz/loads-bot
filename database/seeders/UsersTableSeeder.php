<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        User::factory()->create([
            'name' => 'Administrator',
            'email' => 'admin@loadsbot.uz',
            'password' => bcrypt('password'),
        ]);

        // User::factory()->create([
        //     'name' => 'Test',
        //     'email' => 'test@test.com',
        // ]);

    }
}
