<?php

namespace Database\Seeders;

use App\Models\State;
use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            // Pacific Time Zone
            [
                'code' => 'CA',
                'name' => 'California',
                'timezone_code' => 'PST',
            ],
            [
                'code' => 'OR',
                'name' => 'Oregon',
                'timezone_code' => 'PST',
            ],
            [
                'code' => 'WA',
                'name' => 'Washington',
                'timezone_code' => 'PST',
            ],
            [
                'code' => 'NV',
                'name' => 'Nevada',
                'timezone_code' => 'PST',
            ],
            // Mountain Time Zone
            [
                'code' => 'AZ',
                'name' => 'Arizona',
                'timezone_code' => 'MST',
            ],
            [
                'code' => 'NM',
                'name' => 'New Mexico',
                'timezone_code' => 'MST',
            ],
            [
                'code' => 'UT',
                'name' => 'Utah',
                'timezone_code' => 'MST',
            ],
            [
                'code' => 'CO',
                'name' => 'Colorado',
                'timezone_code' => 'MST',
            ],
            [
                'code' => 'ID',
                'name' => 'Idaho',
                'timezone_code' => 'MST',
            ],
            [
                'code' => 'WY',
                'name' => 'Wyoming',
                'timezone_code' => 'MST',
            ],
            [
                'code' => 'MT',
                'name' => 'Montana',
                'timezone_code' => 'MST',
            ],
            // Central Time Zone
            [
                'code' => 'ND',
                'name' => 'North Dakota',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'SD',
                'name' => 'South Dakota',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'MN',
                'name' => 'Minnesota',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'WI',
                'name' => 'Wisconsin',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'NE',
                'name' => 'Nebraska',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'IA',
                'name' => 'Iowa',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'IL',
                'name' => 'Illinois',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'KS',
                'name' => 'Kansas',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'MO',
                'name' => 'Missouri',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'OK',
                'name' => 'Oklahoma',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'AR',
                'name' => 'Arkansas',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'TX',
                'name' => 'Texas',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'LA',
                'name' => 'Louisiana',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'MS',
                'name' => 'Mississippi',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'AL',
                'name' => 'Alabama',
                'timezone_code' => 'CST',
            ],
            [
                'code' => 'TN',
                'name' => 'Tennessee',
                'timezone_code' => 'CST',
            ],
            // Eastern Time Zone
            [
                'code' => 'MI',
                'name' => 'Michigan',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'IN',
                'name' => 'Indiana',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'OH',
                'name' => 'Ohio',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'KY',
                'name' => 'Kentucky',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'PA',
                'name' => 'Pennsylvania',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'WV',
                'name' => 'West Virginia',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'VA',
                'name' => 'Virginia',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'NC',
                'name' => 'North Carolina',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'SC',
                'name' => 'South Carolina',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'GA',
                'name' => 'Georgia',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'FL',
                'name' => 'Florida',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'DE',
                'name' => 'Delaware',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'ME',
                'name' => 'Maine',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'NH',
                'name' => 'New Hampshire',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'MD',
                'name' => 'Maryland',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'MA',
                'name' => 'Massachusetts',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'VT',
                'name' => 'Vermont',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'RI',
                'name' => 'Rhode Island',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'NJ',
                'name' => 'New Jersey',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'NY',
                'name' => 'New York',
                'timezone_code' => 'EST',
            ],
            // Additional States
            [
                'code' => 'VT',
                'name' => 'Vermont',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'RI',
                'name' => 'Rhode Island',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'NJ',
                'name' => 'New Jersey',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'NY',
                'name' => 'New York',
                'timezone_code' => 'EST',
            ],
            [
                'code' => 'FL',
                'name' => 'Florida',
                'timezone_code' => 'EST',
            ],
        ];

        foreach ($states as $value) {
            State::updateOrCreate([
                'code' => $value['code'],
            ], [
                'name' => $value['name'],
                'timezone_code' => $value['timezone_code'],
            ]);
        }
    }
}
