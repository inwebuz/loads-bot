<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\Models\Driver;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DriverPlanFillCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:fill_plan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Driver::active()->with('driverPlans')->cursor() as $driver) {
            $driverPlan = $driver->driverPlans()->orderBy('id', 'desc')->first();
            if (now()->subHours(3)->startOfWeek() != now()->startOfWeek()) {
                $driverPlan = $driver->driverPlans()->orderBy('id', 'desc')->skip(1)->first();
            }
            if (!$driverPlan) {
                continue;
            }
            $weekStart = $driverPlan->start_time;
            $weekEnd = $driverPlan->end_time;
            $driver->load(['loads' => function($q1) use ($weekStart, $weekEnd) {
                $q1->active()->where('pickup_time', '>=', $weekStart)->where('pickup_time', '<=', $weekEnd);
            }]);
            $weeklyStats = Helper::getDriverWeeklyStats($driver, $weekStart, $weekEnd);
            $driverPlan->update([
                'total' => $weeklyStats['loadsTotal'],
                'rate' => $weeklyStats['loadsRate'],
                'tonu' => $weeklyStats['loadsTonu'],
                'mile' => $weeklyStats['loadsMile'],
                'authorized_loaded_mile' => $weeklyStats['loadsAuthorizedLoadedMile'],
                'deadhead_mile' => $weeklyStats['loadsDeadheadMile'],
                'authorized_mile' => $weeklyStats['loadsAuthorizedMile'],
                'unauthorized_mile' => $weeklyStats['loadsUnauthorizedMile'],
                'per_mile' => $weeklyStats['perMile'],
            ]);
        }
        return 0;
    }
}
