<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\Models\Driver;
use App\Models\DriverPlan;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DriverCreateWeeklyPlanCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:create_weekly_plan {week=next}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create driver weekly plan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $week = $this->argument('week');
        if ($week == 'next') {
            $weekStart = now()->addWeek()->startOfWeek();
            $weekEnd = (clone $weekStart)->endOfWeek();
        } elseif ($week == 'current') {
            $weekStart = now()->startOfWeek();
            $weekEnd = (clone $weekStart)->endOfWeek();
        } else {
            return 0;
        }

        Driver::chunk(200, function($drivers) use ($weekStart, $weekEnd) {
            foreach ($drivers as $driver) {
                $driverPlan = DriverPlan::where('start_time', $weekStart->format('Y-m-d H:i:s'))->where('driver_id', $driver->id)->orderBy('id', 'desc')->first();
                $data = [
                    'target' => $driver->weekly_target,
                    'target_mile' => $driver->weekly_target_mile,
                    'pay_per_mile' => $driver->pay_per_mile,
                    'penske_pay_per_mile' => $driver->penske_pay_per_mile,
                    'fixed_cost' => $driver->fixed_cost,
                    'fuel_price_per_mile' => floatval(Helper::setting('fuel_price_per_mile')),
                    'toll_price_per_mile' => floatval(Helper::setting('toll_price_per_mile')),
                    'start_time' => $weekStart,
                    'end_time' => $weekEnd,
                ];
                if ($driverPlan) {
                    $driverPlan->update($data);
                } else {
                    $driver->driverPlans()->create($data);
                }
            }
        });
        return 0;
    }
}
