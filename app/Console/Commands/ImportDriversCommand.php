<?php

namespace App\Console\Commands;

use App\Models\Driver;
use Illuminate\Console\Command;

class ImportDriversCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:drivers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import drivers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $list = [];

        foreach ($list as $value) {
            Driver::updateOrCreate([
                'name' => $value,
            ], [
                'active' => 1,
            ]);
        }

        return 0;
    }
}
