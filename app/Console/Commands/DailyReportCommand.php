<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\Models\Load;
use App\Services\TelegramService;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class DailyReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create and send daily reports';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $from = now()->subDay()->startOfDay();
        $to = now()->subDay()->endOfDay();

        $query = Load::orderBy('id', 'desc');
        $query->active()
            ->where('pickup_time', '>=', $from)
            ->where('pickup_time', '<=', $to);

        $query->with(['qcBotUser.botUserInfo', 'dspBotUser.botUserInfo']);

        $loadsAll = $query->get();
        $stats = Helper::generateLoadsStats($loadsAll);
        $reportGroupChatId = Helper::setting('report_group');

        $writer = WriterEntityFactory::createXLSXWriter();

        $fileName = 'Loads-' . $from->format('Y-m-d') . '-' . $to->format('Y-m-d') . '.xlsx';
        $filePath = Storage::path('reports/' . $fileName);
        $writer->openToFile($filePath);

        // header
        $headingsRowValues = [];
        $headingsRowValues[] = __('DSP');
        foreach (Load::types() as $key => $value) {
            $headingsRowValues[] = $value;
            $headingsRowValues[] = $value . ' Cancelled';
        }
        $headingsRowValues[] = 'Total Loads';
        $headingsRowValues[] = 'Total Loads Cancelled';

        $row = WriterEntityFactory::createRowFromArray($headingsRowValues);
        $writer->addRow($row);

        // body
        foreach ($stats as $stat) {
            $rowValues = [];
            $rowValues[] = $stat['dsp']->full_name;
            foreach (\App\Models\Load::types() as $key => $value) {
                $rowValues[] = $stat['by_load_types'][$key]['loads'];
                $rowValues[] = $stat['by_load_types'][$key]['loads_cancelled'];
            }
            $rowValues[] = $stat['total_loads'];
            $rowValues[] = $stat['total_loads_cancelled'];

            $row = WriterEntityFactory::createRowFromArray($rowValues);
            $writer->addRow($row);
        }

        $writer->close();

        $telegram = new TelegramService();
        $telegram->sendDocument($reportGroupChatId, $filePath);
        // $telegram->sendDocument(11120017, $filePath);
        return 0;
    }
}
