<?php

namespace App\Console\Commands;

use App\Models\Load;
use Illuminate\Console\Command;

class ChangeDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Load::cursor() as $load) {
            // if ($load->isCancelled()) {
            //     $load->total = $load->tonu;
            // } else {
            //     $load->total;
            // }

            $load->local_pickup_time = $load->pickup_time;
            $load->local_delivery_time = $load->delivery_time;
            $load->pickup_timezone = 'EST';
            $load->delivery_timezone = 'EST';

            $load->saveQuietly();
        }
        return 0;
    }
}
