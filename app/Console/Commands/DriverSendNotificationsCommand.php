<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\Models\BotUserInfo;
use App\Models\Driver;
use App\Models\Load;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DriverSendNotificationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:send_notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Driver send notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Driver::active()->with(['currentDriverStatus.relatedLoad', 'team.managers.botUser'])->chunk(10, function ($drivers) {
            foreach ($drivers as $driver) {
                $message = '';
                $notifyManagersType = '';
                try {
                    $now = now();
                    $driverStatus = $driver->currentDriverStatus;
                    if (!$driverStatus || !$driver->chat_id) {
                        continue;
                    }

                    if ($driverStatus->isShop()) {
                        // TODO: notify fleet team
                    } elseif ($driverStatus->isRest()) {
                        if (
                            ((clone $now)->addMinutes(60) >= $driverStatus->end_time && (clone $now)->addMinutes(55) <= $driverStatus->end_time) ||
                            ((clone $now)->addMinutes(15) >= $driverStatus->end_time && (clone $now)->addMinutes(10) <= $driverStatus->end_time)
                        ) {
                            $notifyManagersType = 'DSP';
                            $message = $driverStatus->status_title . ' ends at ' . $driverStatus->end_time->format('Y-m-d H:i');
                        }
                    } elseif ($driverStatus->isHome() || $driverStatus->isVacation()) {
                        if (
                            ((clone $now)->addMinutes(60) >= $driverStatus->end_time && (clone $now)->addMinutes(55) <= $driverStatus->end_time) ||
                            ((clone $now)->addMinutes(1440) >= $driverStatus->end_time && (clone $now)->addMinutes(1435) <= $driverStatus->end_time) ||
                            ((clone $now)->addMinutes(720) >= $driverStatus->end_time && (clone $now)->addMinutes(715) <= $driverStatus->end_time)
                        ) {
                            $notifyManagersType = 'DSP';
                            $message = $driverStatus->status_title . ' ends at ' . $driverStatus->end_time->format('Y-m-d H:i');
                        }
                    } elseif ($driverStatus->isReady()) {
                        if ((clone $now)->subMinutes(5) <= $driverStatus->start_time && $now >= $driverStatus->start_time) {
                            $notifyManagersType = 'DSP';
                            $message = $driverStatus->status_title . ' started at ' . $driverStatus->start_time->format('Y-m-d H:i');
                        }
                    } elseif ($driverStatus->isDispatched()) {
                        if ((clone $now)->subMinutes(5) <= $driverStatus->start_time && $now >= $driverStatus->start_time) {
                            $message = $driverStatus->status_title . ' started at ' . $driverStatus->start_time->format('Y-m-d H:i') . ($driverStatus->relatedLoad ? "\n" . $driverStatus->relatedLoad->load_and_pcs : '');
                        }
                        if ((clone $now)->addMinutes(30) >= $driverStatus->end_time && (clone $now)->addMinutes(25) <= $driverStatus->end_time) {
                            // notify group transit starts
                            $notifyManagersType = 'QC';
                            $message = 'Transit starts at ' . $driverStatus->end_time->format('Y-m-d H:i') . ($driverStatus->relatedLoad ? "\n" . $driverStatus->relatedLoad->load_and_pcs : '');
                        }
                    } elseif ($driverStatus->isTransit()) {

                        if ((clone $now)->subMinutes(5) <= $driverStatus->start_time && $now >= $driverStatus->start_time) {
                            $message = $driverStatus->status_title . ' started at ' . $driverStatus->start_time->format('Y-m-d H:i') . ($driverStatus->relatedLoad ? "\n" . $driverStatus->relatedLoad->load_and_pcs : '');
                        }
                        if (
                            ((clone $now)->addMinutes(300) >= $driverStatus->end_time && (clone $now)->addMinutes(295) <= $driverStatus->end_time) ||
                            ((clone $now)->addMinutes(120) >= $driverStatus->end_time && (clone $now)->addMinutes(115) <= $driverStatus->end_time)
                        ) {
                            // notify group transit starts
                            $notifyManagersType = 'QC';
                            $message = 'Transit ends at ' . $driverStatus->end_time->format('Y-m-d H:i') . ($driverStatus->relatedLoad ? "\n" . $driverStatus->relatedLoad->load_and_pcs : '');
                        }
                    }

                    if ($message) {

                        // notify driver chat
                        // Helper::toTelegram($message, $driver->chat_id);

                        // notify managers chat
                        $managersGroupChatId = Helper::setting('managers_group');
                        $managers = null;
                        if ($notifyManagersType == 'DSP' && !empty($driver->team->managers)) {
                            $managers = $driver->team->managers->where('dsp_manager', 1);
                        } elseif ($notifyManagersType == 'QC' && $driverStatus->relatedload) {
                            // if (in_array($driverStatus->relatedload->type, [Load::TYPE_AMAZON_DEDICATED, Load::TYPE_AMAZON_SPOT])) {
                            //     $managers = BotUserInfo::where('qc_manager', 1)->where('load_types', 'LIKE', '%"' . $driverStatus->relatedLoad->load_type . '"%')->get();
                            // } else {
                            //     $managers = BotUserInfo::where('qc_manager', 1)->where('company_codes', 'LIKE', '%"' . $driverStatus->relatedLoad->company_code . '"%')->get();
                            // }
                            $managers = BotUserInfo::where('qc_manager', 1)->where('company_codes', 'LIKE', '%"' . $driverStatus->relatedLoad->company_code . '"%')->get();
                        }

                        if ($managers && $managersGroupChatId) {
                            $message .= "\n";
                            $message .= 'Driver - ' . $driver->name . "\n";
                            foreach ($managers as $manager) {
                                if (!empty($manager->botUser->username)) {
                                    $message .= ' @' . $manager->botUser->username;
                                }
                            }
                            Helper::toTelegram($message, $managersGroupChatId);
                        }
                    }
                } catch (\Throwable $th) {
                    Log::error($th->getMessage());
                }
            }
        });
        return 0;
    }
}
