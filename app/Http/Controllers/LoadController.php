<?php

namespace App\Http\Controllers;

use App\Helpers\BotHelper;
use App\Helpers\Helper;
use App\Models\BotUserInfo;
use App\Models\Driver;
use App\Models\DriverStatus;
use App\Models\Load;
use App\Models\Product;
use App\Models\Seller;
use App\Services\TelegramService;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class LoadController extends Controller
{
    public function index(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = $this->generateQuery($request);
        $queryClone = clone $query;
        $loads = $query->paginate(50);
        $dsps = BotUserInfo::where('position', 'DSP')->orderBy('full_name')->get();
        $qcs = BotUserInfo::where('position', 'QC')->orderBy('full_name')->get();
        $drivers = Driver::active()->orderBy('name')->get();
        $shifts = BotUserInfo::shifts();

        $types = Load::types();
        $statuses = Load::statuses();

        $loadsAll = $queryClone->get();
        $stats = Helper::generateLoadsStats($loadsAll);

        return view('loads.index', compact('loads', 'filter', 'dsps', 'qcs', 'drivers', 'shifts', 'types', 'statuses', 'stats'));
    }

    public function download(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = $this->generateQuery($request);
        $loadsAll = $query->get();

        $writer = WriterEntityFactory::createXLSXWriter();
        // $writer = WriterEntityFactory::createODSWriter();
        // $writer = WriterEntityFactory::createCSVWriter();

        $fromTime = is_object($filter['pickup_time_from']) ? $filter['pickup_time_from']->format('Y-m-d') : 'from';
        $toTime = is_object($filter['pickup_time_to']) ? $filter['pickup_time_to']->format('Y-m-d') : 'to';
        $fileName = 'Loads-' . $fromTime . '-' . $toTime . '.xlsx';
        // $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $writer->openToBrowser($fileName); // stream data directly to the browser

        $values = [
            __('Type'),
            // __('Status'),
            __('Company code'),
            __('Load ID'),
            __('Driver name'),
            __('Team'),
            __('Third company name'),
            __('PCS number'),
            __('Pickup status'),
            __('Pickup QC comment'),
            __('Delivery status'),
            __('Delivery QC comment'),

            __('Total $'),
            __('Rate'),
            __('TONU'),

            __('Total mile'),
            __('Authorized loaded mile'),
            __('Load DH mile'),
            __('Authorized mile (PLD, TRL, FLT miles)'),
            __('Unauthorized miles (including Home DH miles)'),

            __('PU address (full)'),
            __('PU time'),
            __('Delivery address (full)'),
            __('Delivery time'),
            __('Special note'),
            __('Check in as'),
            __('Trailer'),
            __('Trailer note'),
            __('Pickup address and time (2, 3, 4 Stops)'),
            __('DSP'),
            __('Assigned DSP'),
            __('QC'),
            __('Created'),
            __('Updated'),
            __('Cancelled'),
        ];
        $rowFromValues = WriterEntityFactory::createRowFromArray($values);
        $writer->addRow($rowFromValues);

        foreach ($loadsAll as $load) {
            $values = [
                $load->type_title,
                // $load->status_title,
                $load->company_code,
                $load->load_id,
                $load->driver_name,
                $load->team_name,
                $load->third_company,
                $load->pcs_number,
                $load->pickup_status,
                $load->pickup_qc_comment,
                $load->delivery_status,
                $load->delivery_qc_comment,

                $load->total,
                $load->rate,
                $load->tonu,

                $load->mile,
                $load->authorized_loaded_mile,
                $load->deadhead_mile,
                $load->authorized_mile,
                $load->unauthorized_mile,

                $load->pickup_location,
                $load->pickup_time ? Helper::formatDateTime($load->pickup_time) : '',
                $load->delivery_location,
                $load->delivery_time ? Helper::formatDateTime($load->delivery_time) : '',
                $load->special_note,
                $load->check_in_as,
                $load->trailer,
                $load->trailer_note,
                $load->pickup_additional_stops,
                $load->dsp_full_name,
                $load->assigned_dsp_full_name,
                $load->qc_full_name,
                Helper::formatDateTime($load->created_at),
                Helper::formatDateTime($load->updated_at),
                $load->isCancelled() ? 'Cancelled' : ''
            ];
            $rowFromValues = WriterEntityFactory::createRowFromArray($values);
            $writer->addRow($rowFromValues);
        }

        $writer->close();
        exit();
    }

    public function report(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = $this->generateQuery($request);
        $loadsAll = $query->get();
        $stats = Helper::generateLoadsStats($loadsAll);
        $reportGroupChatId = Helper::setting('report_group');

        $writer = WriterEntityFactory::createXLSXWriter();

        $fileName = 'Loads-' . $filter['pickup_time_from']->format('Y-m-d') . '-' . $filter['pickup_time_to']->format('Y-m-d') . '.xlsx';
        $filePath = Storage::path('reports/' . $fileName);
        $writer->openToFile($filePath);

        // header
        $headingsRowValues = [];
        $headingsRowValues[] = __('DSP');
        foreach (Load::types() as $key => $value) {
            $headingsRowValues[] = $value;
            $headingsRowValues[] = $value . ' Cancelled';
        }
        $headingsRowValues[] = 'Total Loads';
        $headingsRowValues[] = 'Total Loads Cancelled';

        $row = WriterEntityFactory::createRowFromArray($headingsRowValues);
        $writer->addRow($row);

        // body
        foreach ($stats as $stat) {
            $rowValues = [];
            $rowValues[] = $stat['dsp']->full_name;
            foreach (\App\Models\Load::types() as $key => $value) {
                $rowValues[] = $stat['by_load_types'][$key]['loads'];
                $rowValues[] = $stat['by_load_types'][$key]['loads_cancelled'];
            }
            $rowValues[] = $stat['total_loads'];
            $rowValues[] = $stat['total_loads_cancelled'];

            $row = WriterEntityFactory::createRowFromArray($rowValues);
            $writer->addRow($row);
        }

        $writer->close();

        $telegram = new TelegramService();
        $telegram->sendDocument($reportGroupChatId, $filePath);
        // $telegram->sendDocument(11120017, $filePath);

        return back()->withSuccess('Report has been created');
    }

    public function reportText(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = $this->generateQuery($request);
        $loadsAll = $query->get();
        $stats = Helper::generateLoadsStats($loadsAll);
        $reportGroupChatId = Helper::setting('report_group');
        $text = Helper::loadStatsTelegramText($stats, $filter);
        $telegram = new TelegramService();
        $telegram->sendMessage($reportGroupChatId, $text);

        return back()->withSuccess('Report has been created');
    }

    public function show(Load $load)
    {
        return view('loads.show', compact('load'));
    }

    private function generateQuery(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = Load::orderBy('id', 'desc');

        $query->active();

        foreach ($filter as $key => $value) {
            if ($value === '' || $value === null || $value === '-') {
                continue;
            }
            if ($key == 'created_time_from') {
                $query->where('created_at', '>=', $value);
            }
            if ($key == 'created_time_to') {
                $query->where('created_at', '<=', $value);
            }
            if ($key == 'pickup_time_from') {
                $query->where('pickup_time', '>=', $value);
            }
            if ($key == 'pickup_time_to') {
                $query->where('pickup_time', '<=', $value);
            }
            if (in_array($key, ['load_id', 'pcs_number', 'third_company', 'pickup_status', 'delivery_status'])) {
                $query->where($key, 'LIKE', '%' . $value . '%');
            }
            if (in_array($key, ['dsp_bot_user_id', 'qc_bot_user_id', 'status', 'type', 'driver_id'])) {
                $query->where($key, $value);
            }
            if ($key == 'shift') {
                $query->whereHas('dspBotUser', function ($q1) use ($value) {
                    $q1->whereHas('botUserInfo', function ($q2) use ($value) {
                        $q2->where('shift', $value);
                    });
                });
            }
        }

        $query->with(['qcBotUser.botUserInfo', 'dspBotUser.botUserInfo']);

        return $query;
    }

    private function getFilter(Request $request)
    {
        $inputCreatedTimeFrom = $request->input('created_time_from', '-');
        $inputCreatedTimeTo = $request->input('created_time_to', '-');
        $createdTimeFrom = '';
        $createdTimeTo = '';
        if ($inputCreatedTimeFrom != '-') {
            try {
                $createdTimeFrom = Carbon::createFromFormat('Y-m-d', $inputCreatedTimeFrom)->startOfDay();
            } catch (\Throwable $th) {
                $createdTimeFrom = Carbon::now()->startOfYear();
                //throw $th;
            }
        }
        if ($inputCreatedTimeTo != '-') {
            try {
                $createdTimeTo = Carbon::createFromFormat('Y-m-d', $inputCreatedTimeTo)->endOfDay();
            } catch (\Throwable $th) {
                $createdTimeTo = Carbon::now()->endOfYear();
                //throw $th;
            }
        }

        $inputPickupTimeFrom = $request->input('pickup_time_from', '');
        $inputPickupTimeTo = $request->input('pickup_time_to', '');
        $pickupTimeFrom = '';
        $pickupTimeTo = '';
        if ($inputPickupTimeFrom != '-') {
            try {
                $pickupTimeFrom = Carbon::createFromFormat('Y-m-d', $inputPickupTimeFrom)->startOfDay();
            } catch (\Throwable $th) {
                $pickupTimeFrom = Carbon::now()->startOfYear();
                //throw $th;
            }
        }
        if ($inputPickupTimeTo != '-') {
            try {
                $pickupTimeTo = Carbon::createFromFormat('Y-m-d', $inputPickupTimeTo)->endOfDay();
            } catch (\Throwable $th) {
                $pickupTimeTo = Carbon::now()->endOfYear();
                //throw $th;
            }
        }

        $filter = [
            'created_time_from' => $createdTimeFrom,
            'created_time_to' => $createdTimeTo,
            'pickup_time_from' => $pickupTimeFrom,
            'pickup_time_to' => $pickupTimeTo,
            'load_id' => $request->input('load_id', ''),
            'third_company' => $request->input('third_company', ''),
            'shift' => $request->input('shift', ''),
            'pcs_number' => $request->input('pcs_number', ''),
            'pickup_status' => $request->input('pickup_status', '-'),
            'delivery_status' => $request->input('delivery_status', '-'),
            'qc_bot_user_id' => $request->input('qc_bot_user_id', '-'),
            'dsp_bot_user_id' => $request->input('dsp_bot_user_id', '-'),
            'driver_id' => $request->input('driver_id', '-'),
            'type' => $request->input('type', '-'),
            'status' => $request->input('status', '-'),
        ];

        $dailyReport = $request->input('daily', '');
        if ($dailyReport) {
            $filter['pickup_time_from'] = now()->subDay()->startOfDay();
            $filter['pickup_time_to'] = now()->subDay()->endOfDay();
        }

        return $filter;
    }

    public function edit(Request $request, Load $load)
    {
        Gate::authorize('update-load', $load);
        $user = auth()->user();
        $drivers = Driver::active()->get();
        return view('loads.edit', compact('user', 'load', 'drivers'));
    }

    public function update(Request $request, Load $load)
    {
        Gate::authorize('update-load', $load);
        $user = auth()->user();
        $validationFields = [
            'company_code' => 'required|in:"' . implode('","', Load::companyCodes()) . '"',
            'load_id' => 'required',
            'pcs_number' => 'required|digits:6|unique:loads,pcs_number,' . $load->id,
            'rate' => 'required|numeric|min:0',
            'authorized_loaded_mile' => 'required|numeric|min:0',
            'deadhead_mile' => 'required|numeric|min:0',
            'authorized_mile' => 'required|numeric|min:0',
            'unauthorized_mile' => 'required|numeric|min:0',
            'pickup_location' => 'required|regex:' . Helper::addressRegex(),
            'pickup_timezone' => 'required|in:"' . implode('","', Load::timezones()) . '"',
            'local_pickup_time' => 'required|string|date_format:Y-m-d H:i',
            'delivery_location' => 'required|regex:' . Helper::addressRegex(),
            'delivery_timezone' => 'required|in:"' . implode('","', Load::timezones()) . '"',
            'local_delivery_time' => 'required|string|date_format:Y-m-d H:i|after:local_pickup_time',
            'special_note' => 'required',
            'check_in_as' => 'nullable|max:65000',
            'trailer' => 'nullable|max:65000',
            'trailer_note' => 'nullable|max:65000',
            'pickup_additional_stops' => 'nullable|max:65000',
        ];
        if ($load->isThirdParty()) {
            $validationFields['third_company'] = 'required';
        } else {
            $validationFields['driver_id'] = [
                'required',
                function ($attribute, $value, $fail) {
                    if (Driver::active()->where('id', $value)->count() == 0) {
                        $fail('Driver not found');
                    }
                },
            ];
        }
        if ($user->isQc()) {
            $validationFields['pickup_status'] = 'required|in:"' . implode('","', Load::pickupStatuses()) . '"';
            $validationFields['pickup_qc_comment'] = 'required|in:"' . implode('","', Load::pickupQcComments()) . '"';
            $validationFields['delivery_status'] = 'required|in:"' . implode('","', Load::deliveryStatuses()) . '"';
            $validationFields['delivery_qc_comment'] = 'required|in:"' . implode('","', Load::deliveryQcComments()) . '"';
        }
        $data = $request->validate($validationFields);

        $puDate = Helper::convertToBotTime(Carbon::createFromFormat('Y-m-d H:i', $data['local_pickup_time']), $data['pickup_timezone']);
        $delDate = Helper::convertToBotTime(Carbon::createFromFormat('Y-m-d H:i', $data['local_delivery_time']), $data['delivery_timezone']);

        // check status
        $driver = null;
        if (!$load->isThirdParty()) {
            $driver = Driver::find($data['driver_id']);
            if ($driver) {

                $driverStatuses = DriverStatus::query()
                    ->where('driver_id', $driver->id)
                    ->where(function ($q1) use ($puDate, $delDate) {
                        $q1
                            ->where(function ($q2) use ($puDate) {
                                $q2
                                    ->where('start_time', '<=', $puDate)
                                    ->where(function ($q3) use ($puDate) {
                                        $q3
                                            ->where('end_time', '>=', $puDate)
                                            ->orWhereNull('end_time');
                                    });
                            })
                            ->orWhere(function ($q2) use ($puDate, $delDate) {
                                $q2
                                    ->where('start_time', '>=', $puDate)
                                    ->where('start_time', '<=', $delDate);
                            });
                    })
                    ->get();
                foreach ($driverStatuses as $driverStatus) {
                    if (in_array($driverStatus->status, DriverStatus::loadCreateUnavailableStatuses())) {
                        return back()->withError('Can not edit load - PU time ' . $data['local_pickup_time'] . '. Status is ' . $driverStatus->status_title . '. Status ID: ' . $driverStatus->id . '. Start time: ' . $driverStatus->start_time . ', End time: ' . $driverStatus->end_time . '. Contact admin to fix the error');
                    }
                }
            }
        }

        $data['third_company'] = $data['third_company'] ?? null;

        session()->put('load', [
            'id' => $load->id,
            'data' => $data,
        ]);

        return redirect()->route('loads.edit.confirm', [$load->id]);
    }

    public function editConfirmCreate(Request $request, Load $load)
    {
        Gate::authorize('update-load', $load);
        $user = auth()->user();
        $sessionLoad = session()->get('load');
        if (empty($sessionLoad['id']) || $sessionLoad['id'] != $load->id) {
            return redirect()->route('loads.edit', [$load->id]);
        }
        $driver = null;
        if ($sessionLoad['data']['driver_id']) {
            $driver = Driver::find($sessionLoad['data']['driver_id']);
        }

        $puDate = Helper::convertToBotTime(Carbon::createFromFormat('Y-m-d H:i', $sessionLoad['data']['local_pickup_time']), $sessionLoad['data']['pickup_timezone']);
        $delDate = Helper::convertToBotTime(Carbon::createFromFormat('Y-m-d H:i', $sessionLoad['data']['local_delivery_time']), $sessionLoad['data']['delivery_timezone']);

        $transitDays = $puDate->diffInDays($delDate);
        if ($transitDays > Load::TRANSIT_MAX_DAYS) {
            session()->flash('warning', 'Transit days too long - ' . $transitDays . ' days');
        }
        return view('loads.edit-confirm', compact('user', 'load', 'sessionLoad', 'driver'));
    }

    public function editConfirmStore(Request $request, Load $load)
    {
        Gate::authorize('update-load', $load);
        $user = auth()->user();
        $sessionLoad = session()->get('load');
        if (empty($sessionLoad['id']) || $sessionLoad['id'] != $load->id) {
            return redirect()->route('loads.edit', [$load->id]);
        }

        $sessionLoad['data']['edited_times'] = $load->edited_times + 1;

        // update
        $load->update($sessionLoad['data']);

        // clear session
        session()->forget('load');

        // notify history group
        $historyGroupChatId = Helper::setting('history_group');
        Helper::toTelegram('Load has been edited' . "\n" . $load->generateInfoTelegram(), $historyGroupChatId);

        return redirect()->route('loads.show', [$load->id])->withSuccess(__('Load has been edited'));
    }

    public function cancelCreate(Request $request, Load $load)
    {
        Gate::authorize('cancel-load', $load);
        return view('loads.cancel', compact('load'));
    }

    public function cancelStore(Request $request, Load $load)
    {
        Gate::authorize('cancel-load', $load);
        $data = $request->validate([
            'cancel_time' => 'required|string|date_format:Y-m-d H:i',
            'tonu' => 'required|numeric|min:0',
            'cancel_comment' => 'required|max:50000',
        ]);

        $load->update($data);
        $load->setCancelled();

        return redirect()->route('loads.show', [$load->id])->withSuccess(__('Load has been cancelled'));
    }

    public function destroy(Load $load)
    {
        Gate::authorize('delete-load', $load);
        $load->delete();
        return redirect()->route('loads.index')->with('success', 'Load deleted');
    }
}
