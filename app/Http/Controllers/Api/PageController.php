<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function updateTableInfo(Request $request)
    {
        $data = $request->validate([
            'table' => 'required|in:drivers,driver_plans,bot_user_info',
            'field' => 'required|in:trailer_info,pm_info,dsp_comment,manager_comment,teamsheet_info',
            'id' => 'required',
            'description' => 'required|max:65000',
        ]);
        DB::table($data['table'])->where('id', $data['id'])->update([
            $data['field'] => $data['description'],
        ]);
        if (session()->has('latest-cache-key')) {
            Cache::forget(session()->get('latest-cache-key'));
        }
        return response()->json([
            'status' => 'success',
            'error_code' => 0,
            'message' => 'Data saved',
        ]);
    }
}
