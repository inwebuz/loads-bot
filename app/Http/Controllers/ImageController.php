<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ImageController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'image|max:2048',
        ]);
        if ($validator->fails()) {
            // return response()->json($validator->errors(), 422);
            return response(__('Error'), 422);
        }

        $file = $request->file('file');
        $path = $file->store('images', 'public');
        $image = Image::create([
            'path' => $path,
            'type' => $file->extension(),
            'size' => $file->getSize(),
            'original_name' => $file->getClientOriginalName(),
        ]);

        return $image;
    }
}
