<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\BotUser;
use App\Models\BotUserInfo;
use App\Models\Changelog;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use stdClass;

class UserController extends Controller
{
    private $page = '/users';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = $this->generateQuery($request);
        $users = $query->paginate(50);
        return view('users.index', compact('users', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $botUsers = BotUser::whereHas('botUserInfo.botUser')->orderBy('first_name')->get();
        $roles = User::roles();
        return view('users.create', compact('user', 'botUsers', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validatedData($request);
        $data['active'] = !empty($data['active']) ? 1 : 0;
        $data['password'] = Hash::make($data['password']);
        $data['phone_number'] = $data['bot_user_info_id'] . mt_rand(100000, 999999);
        $user = User::create($data);
        return redirect($this->page)->with('success', 'User saved');
    }

    /**
     * Display the specified resource.
     *
     * @param int
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $botUsers = BotUser::whereHas('botUserInfo')->orderBy('first_name')->get();
        $roles = User::roles();
        return view('users.edit', compact('user', 'botUsers', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $this->validatedData($request, $user);
        $data['active'] = !empty($data['active']) ? 1 : 0;
        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }
        $user->update($data);
        return redirect($this->page)->with('success', 'User saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect($this->page)->with('success', 'User deleted');
    }

    private function validatedData(Request $request, User $user = null)
    {
        $validation = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|max:255',
            'bot_user_info_id' => 'required|exists:bot_user_info,id',
            'role_id' => 'required|in:' . implode(',', array_keys(User::roles())),
            'active' => '',
        ];
        if ($user) {
            $validation['password'] = 'max:255';
            $validation['email'] .= ',' . $user->id;
        }
        return $request->validate($validation);
    }

    private function generateQuery(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = User::orderBy('id', 'desc');
        foreach ($filter as $key => $value) {
            if ($value === '' || $value === null || $value === '-') {
                continue;
            }
            if (in_array($key, ['name', 'email'])) {
                $query->where($key, 'LIKE', '%' . $value . '%');
            }
        }
        $query->with(['botUserInfo']);
        return $query;
    }

    private function getFilter(Request $request)
    {
        $filter = [
            'name' => $request->input('name', ''),
            'email' => $request->input('email', ''),
        ];
        return $filter;
    }
}
