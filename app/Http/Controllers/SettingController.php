<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    private $table = 'settings';
    private $page = '/settings';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $settings = Setting::where('visibility', 1)->orderBy('id', 'desc')->paginate(50);

        return view('settings.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = new Setting();
        return view('settings.create', compact('setting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validatedData($request);
        Setting::create($data);
        return redirect($this->page)->with('success', 'Setting saved');
    }

    /**
     * Display the specified resource.
     *
     * @param int
     * @return \Illuminate\Http\Response
     */
    public function show($setting)
    {
        return view('settings.show', compact('setting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $setting
     * @return \Illuminate\Http\Response
     */
    public function edit($settingID)
    {
        $setting = Setting::findOrFail($settingID);
        return view('settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $settingID)
    {
        $data = $this->validatedData($request);
        $setting = Setting::findOrFail($settingID);
        $setting->update($data);
        return redirect($this->page)->with('success', 'Setting saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy($settingID)
    {
        $setting = Setting::findOrFail($settingID);
        $setting->delete();
        return redirect($this->page)->with('success', 'Setting deleted');
    }

    private function validatedData(Request $request)
    {
        return $request->validate([
            'key' => 'required|max:255',
            'name' => 'required|max:255',
            'description' => 'required|max:4000',
        ]);
    }
}
