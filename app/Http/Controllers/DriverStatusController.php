<?php

namespace App\Http\Controllers;

use App\Models\Driver;
use App\Models\DriverStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class DriverStatusController extends Controller
{
    public function index(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = $this->generateQuery($request);
        $driverStatuses = $query->paginate(50);
        $statuses = DriverStatus::statuses();
        $drivers = Driver::select(['id', 'name'])->active()->get();
        return view('driver-statuses.index', compact('driverStatuses', 'statuses', 'filter', 'drivers'));
    }

    public function edit(Request $request, DriverStatus $driverStatus) {
        Gate::authorize('update-driver-status', $driverStatus);
        return view('driver-statuses.edit', compact('driverStatus'));
    }

    public function update(Request $request, DriverStatus $driverStatus) {
        Gate::authorize('update-driver-status', $driverStatus);
        $data = $request->validate([
            'start_time' => 'required|string|date_format:Y-m-d H:i',
            'end_time' => 'nullable|string|date_format:Y-m-d H:i|after:start_time',
        ]);
        $driverStatus->update($data);
        return redirect()->route('driver-statuses.index', ['driver_id' => $driverStatus->driver_id])->withSuccess('Data saved');
    }

    public function destroy(DriverStatus $driverStatus)
    {
        Gate::authorize('delete-load', $driverStatus);
        $driverId = $driverStatus->driver_id;
        $driverStatus->delete();
        return redirect()->route('driver-statuses.index', ['driver_id' => $driverId])->withSuccess('Data deleted');
    }

    private function generateQuery(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = DriverStatus::orderBy('id', 'desc');
        foreach ($filter as $key => $value) {
            if ($value === '' || $value === null || $value === '-') {
                continue;
            }
            if (in_array($key, ['hashtag'])) {
                $query->where($key, 'LIKE', '%' . $value . '%');
            }
            if (in_array($key, ['id', 'status', 'driver_id'])) {
                $query->where($key, $value);
            }
        }
        $query->with(['driver', 'relatedload']);
        return $query;
    }

    private function getFilter(Request $request)
    {
        $filter = [
            'id' => $request->input('id', ''),
            'status' => $request->input('status', '-'),
            'driver_id' => $request->input('driver_id', ''),
        ];
        return $filter;
    }
}
