<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\BotUser;
use App\Models\BotUserInfo;
use App\Models\Changelog;
use App\Models\Driver;
use App\Models\DriverStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use stdClass;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = $this->generateQuery($request);
        $drivers = $query->paginate(50);
        $activeDriversQty = Driver::active()->count();
        $statuses = DriverStatus::statuses();
        return view('drivers.index', compact('drivers', 'statuses', 'filter', 'activeDriversQty'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $driver = new Driver();
        $dispatchers = BotUser::whereHas('botUserInfo', function ($q) {
            $q->where('position', BotUserInfo::POSITION_DSP);
        })->get();
        return view('drivers.create', compact('driver', 'dispatchers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validatedData($request);
        $data['active'] = !empty($data['active']) ? 1 : 0;
        $dispatcher_ids = [];
        if (!empty($data['dispatcher_ids'])) {
            $dispatcher_ids = $data['dispatcher_ids'];
        }
        unset($data['dispatcher_ids']);
        $driver = Driver::create($data);
        $driver->additionalDispatchers()->sync($dispatcher_ids);

        // create weekly plan
        $weekStart = now()->startOfWeek();
        $weekEnd = (clone $weekStart)->endOfWeek();
        $data = [
            'target' => $driver->weekly_target,
            'target_mile' => $driver->weekly_target_mile,
            'pay_per_mile' => $driver->pay_per_mile,
            'penske_pay_per_mile' => $driver->penske_pay_per_mile,
            'fixed_cost' => $driver->fixed_cost,
            'fuel_price_per_mile' => floatval(Helper::setting('fuel_price_per_mile')),
            'toll_price_per_mile' => floatval(Helper::setting('toll_price_per_mile')),
            'start_time' => $weekStart,
            'end_time' => $weekEnd,
        ];
        $driver->driverPlans()->create($data);


        return redirect()->route('drivers.index')->with('success', 'Driver saved');
    }

    /**
     * Display the specified resource.
     *
     * @param int
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        return view('drivers.show', compact('driver'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        $dispatchers = BotUser::whereHas('botUserInfo', function ($q) {
            $q->where('position', BotUserInfo::POSITION_DSP);
        })->get();
        return view('drivers.edit', compact('driver', 'dispatchers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Driver $driver)
    {
        $data = $this->validatedData($request, $driver);
        $data['active'] = !empty($data['active']) ? 1 : 0;
        $dispatcher_ids = [];
        if (!empty($data['dispatcher_ids'])) {
            $dispatcher_ids = $data['dispatcher_ids'];
        }
        unset($data['dispatcher_ids']);
        $driver->update($data);
        $driver->additionalDispatchers()->sync($dispatcher_ids);
        return redirect()->route('drivers.index')->with('success', 'Driver saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        if ($driver->isActive()) {
            return back()->with('error', 'Can not delete - driver is active');
        }
        $driver->delete();
        return redirect()->route('drivers.index')->with('success', 'Driver deleted');
    }

    private function validatedData(Request $request, Driver $driver = null)
    {
        $validation = [
            'name' => 'required|max:255|not_regex:/\s{2,}/i|unique:drivers,name',
            'type' => 'required|in:' . implode(',', array_keys(Driver::types())),
            'telegram_username' => 'nullable|max:255',
            'phone_number' => 'required|max:50',
            'dispatcher_id' => 'required|exists:bot_user,id',
            'dispatcher_ids' => 'array',
            'weekly_target' => 'required|numeric|min:0',
            'weekly_target_mile' => 'required|numeric|min:0',
            'truck_number' => 'required|max:5000|unique:drivers,truck_number',
            'pay_per_mile' => 'required|numeric|min:0',
            'penske_pay_per_mile' => 'required|numeric|min:0',
            'fixed_cost' => 'required|numeric|min:0',
            'home_address' => 'required|regex:' . Helper::addressRegex(),
            'active' => '',
        ];
        if ($driver) {
            $validation['name'] .= ',' . $driver->id;
            $validation['truck_number'] .= ',' . $driver->id;
        }
        return $request->validate($validation);
    }

    public function stats(Request $request, Driver $driver)
    {
        $filter = [];
        $currentYear = date('Y');
        $filter['year'] = $request->input('year', $currentYear);
        try {
            $date = Carbon::createFromDate($filter['year']);
        } catch (\Throwable $th) {
            abort(400);
        }
        $weeks = [];
        for ($i = 1; $i <= $date->weeksInYear; $i++) {
            $weeks[] = $i;
        }
        $now = now();
        $currentWeek = $now->weekOfYear;
        $filter['week'] = $request->input('week', $currentWeek);
        if ($filter['week'] < 1) {
            $filter['week'] = 1;
        } elseif ($filter['week'] > $date->weeksInYear) {
            $filter['week'] = $date->weeksInYear;
        }
        $years = [];
        for ($i = $currentYear; $i >= config('services.cargoprime.start_year'); $i--) {
            $years[] = $i;
        }
        try {
            $weekStart = now()->setISODate($filter['year'], $filter['week'])->startOfWeek();
        } catch (\Throwable $th) {
            abort(400);
        }
        $weekEnd = (clone $weekStart)->endOfWeek();

        $stats = Helper::getDriverWeeklyStats($driver, $weekStart, $weekEnd);

        return view('drivers.stats', compact('driver', 'filter', 'years', 'weeks', 'weekStart', 'weekEnd', 'stats'));
    }

    public function history(Driver $driver)
    {
        $changelogs = Changelog::where('driver_id', $driver->id)->orderBy('id', 'desc')->get();
        return view('drivers.history', compact('driver', 'changelogs'));
    }

    private function generateQuery(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = Driver::orderBy('id', 'desc');
        foreach ($filter as $key => $value) {
            if ($value === '' || $value === null || $value === '-') {
                continue;
            }
            if (in_array($key, ['name', 'truck_number'])) {
                $query->where($key, 'LIKE', '%' . $value . '%');
            }
            if (in_array($key, ['id', 'status'])) {
                $query->where($key, $value);
            }
        }
        $query->with(['dispatcher', 'additionalDispatchers', 'driverStatuses.relatedload']);
        return $query;
    }

    private function getFilter(Request $request)
    {
        $filter = [
            'id' => $request->input('id', ''),
            'name' => $request->input('name', ''),
            'truck_number' => $request->input('truck_number', ''),
            'status' => $request->input('status', '-'),
        ];
        return $filter;
    }
}
