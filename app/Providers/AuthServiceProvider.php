<?php

namespace App\Providers;

use App\Models\DriverStatus;
use App\Models\Load;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            if ($user->isAdmin()) {
                return true;
            }
        });

        Gate::define('update-driver-status', function (User $user, DriverStatus $driverStatus) {
            return false;
        });

        Gate::define('delete-driver-status', function (User $user, DriverStatus $driverStatus) {
            return false;
        });

        Gate::define('update-load', function (User $user, Load $load) {
            if ($user->isQc()) {
                return true;
            }
            if ($load->isCancelled()) {
                return false;
            }
            if (now()->startOfWeek() > $load->pickup_time) {
                return false;
            }

            $botUserInfo = $user->botUserInfo;
            $loadBotUserInfo = null;
            if (!empty($load->driver->dispatcher->botUserInfo)) {
                $loadBotUserInfo = $load->driver->dispatcher->botUserInfo;
            }
            if (!$botUserInfo || !$loadBotUserInfo || $botUserInfo->id != $loadBotUserInfo->id) {
                return false;
            }
            return true;
        });

        Gate::define('cancel-load', function (User $user, Load $load) {

            if ($load->isCancelled()) {
                return false;
            }
            if ($user->isManager() && now()->subWeek()->startOfWeek() > $load->pickup_time) {
                return false;
            } elseif (now()->startOfWeek() > $load->pickup_time) {
                return false;
            }

            $botUserInfo = $user->botUserInfo;
            $loadBotUserInfo = null;
            if (!empty($load->driver->dispatcher->botUserInfo)) {
                $loadBotUserInfo = $load->driver->dispatcher->botUserInfo;
            }
            if (!$botUserInfo || !$loadBotUserInfo || $botUserInfo->id != $loadBotUserInfo->id) {
                return false;
            }
            return true;
        });

        Gate::define('update-kpi-board-dsp-comment', function (User $user) {
            if ($user->isManager() || $user->isDsp()) {
                return true;
            }
            return false;
        });

        Gate::define('update-kpi-board-manager-comment', function (User $user) {
            if ($user->isManager()) {
                return true;
            }
            return false;
        });

    }
}
