<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getDescriptionForTelegramAttribute()
    {
        $descr = $this->description;
        $descr = str_replace('<p>', '', $descr);
        // $descr = str_replace('</p>', '<br />' , $descr);
        $descr = str_replace('</p>', "\n", $descr);
        $descr = str_replace('&nbsp;', ' ' , $descr);
        return strip_tags($descr, '<b><strong><i><em><s><del><u><a><br>');
    }
}
