<?php

namespace App\Models;

use App\Helpers\Helper;
use App\Services\TelegramService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Load extends Model
{
    use HasFactory;

    const TYPE_MARKET_SPOT = 0;
    const TYPE_AMAZON_SPOT = 1;
    const TYPE_MARKET_DEDICATED = 2;
    const TYPE_AMAZON_DEDICATED = 3;
    const TYPE_LOAD_REPOWERED_SELF_TRUCK = 4;
    const TYPE_LOAD_REPOWERED_THIRD_PARTY = 5;
    const TYPE_LOAD_RECOVERED_SELF_TRUCK = 6;
    const TYPE_LOAD_RECOVERED_THIRD_PARTY = 7;

    const STATUS_OVERWRITTEN = -2;
    const STATUS_CANCELLED = -1;
    const STATUS_CREATED = 0;
    const STATUS_CONFIRMED = 1;
    const STATUS_RECONFIRMED = 2;

    const FILE_TYPE_PHOTO = 0;
    const FILE_TYPE_DOCUMENT = 1;

    const TRANSIT_MAX_DAYS = 3;

    protected $guarded = [];

    protected $casts = [
        'local_pickup_time' => 'datetime',
        'pickup_time' => 'datetime',
        'local_delivery_time' => 'datetime',
        'delivery_time' => 'datetime',
        'pickup_time_for_driver' => 'datetime',
        'delivery_time_for_driver' => 'datetime',
    ];

    protected static function booted()
    {
        static::saving(function ($load) {
            $load->mile = $load->countTotalMile();
            $load->total = $load->countTotalRate();
            $load->pickup_time = Helper::convertToBotTime($load->local_pickup_time, $load->pickup_timezone);
            $load->delivery_time = Helper::convertToBotTime($load->local_delivery_time, $load->delivery_timezone);

            $load->batch_number = Helper::getLoadBatchNumber($load);

            $pickupAddressParsed = Helper::parseAddress($load->pickup_location);
            $load->pickup_state_code = $pickupAddressParsed['state'];
            $load->pickup_zip_code = $pickupAddressParsed['zip'];

            $deliveryAddressParsed = Helper::parseAddress($load->delivery_location);
            $load->delivery_state_code = $deliveryAddressParsed['state'];
            $load->delivery_zip_code = $deliveryAddressParsed['zip'];
        });

        static::created(function ($load) {
            // send notification to history group
            $telegram = new TelegramService();
            // send file photo/pdf
            $historyGroupChatId = Helper::setting('history_group');
            if ($load->file_type == Load::FILE_TYPE_PHOTO) {
                $telegram->sendPhoto($historyGroupChatId, $load->file_id);
            } elseif ($load->file_type == Load::FILE_TYPE_DOCUMENT) {
                $telegram->sendDocumentById($historyGroupChatId, $load->file_id);
            }
            // send message
            $telegram->sendMessage($historyGroupChatId, $load->generateInfoTelegram());


            // send notifications to driver group (dispatch confirmation) #LoadID and #MileageInfo
            $dispatchConfirmationText = $load->dispatchInfoTelegram();
            $testingGroupChatId = Helper::setting('temporary_testing_group');
            $driverGroupChatId = $load->driver ? $load->driver->chat_id : null;
            $telegram->sendMessage($testingGroupChatId, $dispatchConfirmationText, 'HTML');
            if ($driverGroupChatId) {
                $telegram->sendMessage($driverGroupChatId, $dispatchConfirmationText, 'HTML');
            }
            $mileageInfoText = $load->mileageInfoTelegram();
            $telegram->sendMessage($testingGroupChatId, $mileageInfoText, 'HTML');
            if ($driverGroupChatId) {
                $telegram->sendMessage($driverGroupChatId, $mileageInfoText, 'HTML');
            }

            /**#MileageInfo
Batch No.: 328

Authorized loaded mile:
DH mile:
Other miles:
______________________________
Total miles:

@DriverName
 */

        });
        static::saved(function ($load) {
            // update driver status after cancelled
            if ($load->cancel_time) {
                if ($load->cancel_time <= $load->pickup_time) {
                    DriverStatus::where('driver_id', $load->driver_id)->where('load_id', $load->id)->where('status', DriverStatus::STATUS_TRANSIT)->delete();
                } elseif ($load->cancel_time > $load->pickup_time && $load->cancel_time < $load->delivery_time) {
                    DriverStatus::where('driver_id', $load->driver_id)->where('load_id', $load->id)->where('status', DriverStatus::STATUS_TRANSIT)->update([
                        'end_time' => $load->cancel_time,
                    ]);
                }
            }

            // update transit driver status if pickup_time and
            $transitDriverStatus = DriverStatus::where('driver_id', $load->driver_id)->where('load_id', $load->id)->where('status', DriverStatus::STATUS_TRANSIT)->latest()->first();
            if ($transitDriverStatus) {
                $transitDriverStatus->start_time = $load->pickup_time;
                $transitDriverStatus->end_time = $load->delivery_time;
                $transitDriverStatus->saveQuietly();
            }
            $dispatchedDriverStatus = DriverStatus::where('driver_id', $load->driver_id)->where('load_id', $load->id)->where('status', DriverStatus::STATUS_DISPATCHED)->latest()->first();
            if ($dispatchedDriverStatus) {
                $dispatchedDriverStatus->end_time = $load->pickup_time;
                $dispatchedDriverStatus->saveQuietly();
            }
        });
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function mainTrailer()
    {
        return $this->belongsTo(Trailer::class, 'trailer_id');
    }

    public function getDriverNameAttribute()
    {
        $name = '-';
        if ($this->driver) {
            $name = $this->driver->name;
        }
        return $name;
    }

    public function assignedDspBotUser()
    {
        return $this->belongsTo(BotUser::class, 'assigned_dsp_bot_user_id');
    }

    public function dspBotUser()
    {
        return $this->belongsTo(BotUser::class, 'dsp_bot_user_id');
    }

    public function qcBotUser()
    {
        return $this->belongsTo(BotUser::class, 'qc_bot_user_id');
    }

    public function isPickedUpOntime()
    {
        return $this->pickup_status == 'Ontime';
    }

    public function isDeliveredOntime()
    {
        return $this->delivery_status == 'Ontime';
    }

    public function isNotPickedUpOntime()
    {
        return $this->pickup_status == 'Late';
    }

    public function isNotDeliveredOntime()
    {
        return $this->delivery_status == 'Late';
    }

    public function getQcFullNameAttribute()
    {
        $name = '-';
        $botUser = $this->qcBotUser;
        if ($botUser && $botUser->botUserInfo) {
            $name = $botUser->botUserInfo->full_name;
        }
        return $name;
    }

    public function getDspFullNameAttribute()
    {
        $name = '-';
        $botUser = $this->dspBotUser;
        if ($botUser && $botUser->botUserInfo) {
            $name = $botUser->botUserInfo->full_name;
        }
        return $name;
    }

    public function getAssignedDspFullNameAttribute()
    {
        $name = '-';
        $botUser = $this->assignedDspBotUser;
        if ($botUser && $botUser->botUserInfo) {
            $name = $botUser->botUserInfo->full_name;
        }
        return $name;
    }

    public function getLoadAndPcsAttribute()
    {
        return $this->load_id . ' (' . $this->pcs_number . ')';
    }

    public static function types()
    {
        return [
            static::TYPE_MARKET_SPOT => __('Market load (spot)'),
            static::TYPE_MARKET_DEDICATED => __('Market load (dedicated)'),
            static::TYPE_AMAZON_SPOT => __('Amazon load (spot)'),
            static::TYPE_AMAZON_DEDICATED => __('Amazon load (dedicated)'),
            static::TYPE_LOAD_REPOWERED_SELF_TRUCK => __('Load repowered (self-truck)'),
            static::TYPE_LOAD_REPOWERED_THIRD_PARTY => __('Load repowered (third party)'),
            static::TYPE_LOAD_RECOVERED_SELF_TRUCK => __('Load recovered (self-truck)'),
            static::TYPE_LOAD_RECOVERED_THIRD_PARTY => __('Load recovered (third party)'),
        ];
    }

    public function getTypeTitleAttribute()
    {
        return static::types()[$this->type] ?? '-';
    }

    public static function statuses()
    {
        return [
            static::STATUS_CREATED => __('Created'),
            static::STATUS_RECONFIRMED => __('Reconfirmed'),
            static::STATUS_CANCELLED => __('Cancelled'),
        ];
    }

    public function getStatusTitleAttribute()
    {
        return static::statuses()[$this->status] ?? '-';
    }

    public function setCancelled()
    {
        $this->status = static::STATUS_CANCELLED;
        $this->save();
    }

    public function isCancelled()
    {
        return $this->status == static::STATUS_CANCELLED;
    }

    public function generateInfoTelegram()
    {
        $text = '';
        $text .= __('DSP') . ': ' . $this->dsp_full_name . PHP_EOL;
        $text .= __('Type') . ': ' . $this->type_title . PHP_EOL;
        $text .= __('Company code') . ': ' . $this->company_code . PHP_EOL;
        $text .= __('Load ID') . ': ' . $this->load_id . PHP_EOL;
        $text .= __('PCS number') . ': ' . $this->pcs_number . PHP_EOL;
        $text .= __('Rate') . ': ' . $this->rate . PHP_EOL;

        $text .= __('Authorized loaded mile') . ': ' . $this->authorized_loaded_mile . PHP_EOL;
        $text .= __('Load DH mile') . ': ' . $this->deadhead_mile . PHP_EOL;
        $text .= __('Authorized mile (PLD, TRL, FLT miles)') . ': ' . $this->authorized_mile . PHP_EOL;
        $text .= __('Unauthorized miles (including Home DH miles)') . ': ' . $this->unauthorized_mile . PHP_EOL;

        $text .= __('PU address (full)') . ': ' . $this->pickup_location . PHP_EOL;
        $text .= __('PU time Rate CON') . ': ' . ($this->pickup_time ? Helper::formatDateTime($this->pickup_time) : '-') . PHP_EOL;
        $text .= __('PU time for driver') . ': ' . ($this->pickup_time_for_driver ? Helper::formatDateTime($this->pickup_time_for_driver) : '-') . PHP_EOL;
        $text .= __('Delivery address (full)') . ': ' . $this->delivery_location . PHP_EOL;
        $text .= __('Delivery time Rate CON') . ': ' . ($this->delivery_time ? Helper::formatDateTime($this->delivery_time) : '-') . PHP_EOL;
        $text .= __('Delivery time for driver') . ': ' . ($this->delivery_time_for_driver ? Helper::formatDateTime($this->delivery_time_for_driver) : '-') . PHP_EOL;
        $text .= __('Created') . ': ' . Helper::formatDateTime($this->created_at) . PHP_EOL;
        if ($this->isCancelled()) {
            $text .= __('Status') . ': ' . $this->status_title . PHP_EOL;
            $text .= __('TONU') . ': ' . $this->tonu . PHP_EOL;
            $text .= __('Cancel comment') . ': ' . $this->cancel_comment . PHP_EOL;
        }
        if (!$this->isThirdParty()) {
            $text .= __('Driver name') . ': ' . $this->driver_name . PHP_EOL;
        }
        if ($this->isThirdParty()) {
            $text .= __('Third company name') . ': ' . $this->third_company . PHP_EOL;
        }
        $text .= __('Special note') . ': ' . $this->special_note . PHP_EOL;
        $text .= __('Check in as') . ': ' . $this->check_in_as . PHP_EOL;
        $text .= __('Trailer') . ': ' . $this->trailer . PHP_EOL;
        $text .= __('Trailer note') . ': ' . $this->trailer_note . PHP_EOL;
        $text .= __('Pickup address and time (2, 3, 4 Stops)') . ': ' . $this->pickup_additional_stops . PHP_EOL;
        if ($this->pickup_status) {
            $text .= __('Pickup status') . ': ' . $this->pickup_status . PHP_EOL;
        }
        if ($this->pickup_qc_comment) {
            $text .= __('Pickup QC comment') . ': ' . $this->pickup_qc_comment . PHP_EOL;
        }
        if ($this->delivery_status) {
            $text .= __('Delivery status') . ': ' . $this->delivery_status . PHP_EOL;
        }
        if ($this->delivery_qc_comment) {
            $text .= __('Delivery QC comment') . ': ' . $this->delivery_qc_comment . PHP_EOL;
        }
        return $text;
    }

    public function dispatchInfoTelegram()
    {
        $text = '';

        $text .= '#LoadID' . PHP_EOL;
        $companyDriver = ($this->driver && $this->driver->isCompanyDriver()) ? true : false;
        if (!in_array($this->company_code, ['AUZBI-D', 'AUZBI-S'])) {
            $text .= '<b>' . __('Load ID') . ': ' . $this->load_id . '</b>' . PHP_EOL;
        }
        $text .= __('PCS number') . ': ' . $this->pcs_number . PHP_EOL;
        if ($this->reference_no) {
            $text .= __('Reference №') . ': ' . $this->reference_no . PHP_EOL;
        }

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;

        if (!$companyDriver) {
            $text .= __('Rate') . ': ' . $this->rate . PHP_EOL;
        }
        $text .= __('Authorized loaded mile') . ': ' . $this->authorized_loaded_mile . PHP_EOL;
        $text .= __('DH mile') . ': ' . $this->deadhead_mile . PHP_EOL;
        $text .= __('Authorized miles') . ': ' . $this->authorized_mile . PHP_EOL;

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;

        $text .= __('Total miles') . ': ' . $this->mile . PHP_EOL;

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;

        $text .= '<b>' . __('PU') . '</b>' . PHP_EOL;
        $text .= __('PU address (full)') . ': ' . $this->pickup_location . PHP_EOL;
        $text .= __('PU time') . ': ' . Helper::formatDateTime($this->pickup_time_for_driver) . PHP_EOL;
        if ($this->pickup_additional_stops) {
            $text .= __('PU address and time (2, 3, 4 Stops)') . ': ' . $this->pickup_additional_stops . PHP_EOL;
        }

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;

        $text .= '<b>' . __('DEL') . '</b>' . PHP_EOL;
        $text .= __('Delivery address (full)') . ': ' . $this->delivery_location . PHP_EOL;
        $text .= __('Delivery time') . ': ' . Helper::formatDateTime($this->delivery_time_for_driver) . PHP_EOL;

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;

        $text .= '<b>' . __('Check in as') . '</b>' . ': ' . $this->check_in_as . PHP_EOL;

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;

        $text .= '<b>' . __('TRAILER') . '</b>' . PHP_EOL;
        $text .= __('Trailer') . ': ' . $this->trailer . PHP_EOL;
        $text .= __('Trailer note') . ': ' . $this->trailer_note . PHP_EOL;

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;

        $text .= '<b>' . __('Warnings (general)') . '</b>' . PHP_EOL;
        $text .= Helper::setting('dispatch_warnings_text') . PHP_EOL;

        if ($this->warnings) {
            $text .= '<b>' . __('Warnings (specific)') . '</b>' . ': ' . $this->warnings . PHP_EOL;
        }

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;

        $text .= '<b>' . __('Dispatch confirmation') . '</b>' . PHP_EOL;
        $text .= Helper::setting('dispatch_confirmation_text') . PHP_EOL;

        return $text;
    }

    public function mileageInfoTelegram()
    {
        $text = '';

        $text .= '#MileageInfo' . PHP_EOL;
        $text .= __('Batch No.') . ': ' . $this->batch_number . PHP_EOL;

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;
        $text .= __('Authorized loaded mile') . ': ' . $this->authorized_loaded_mile . PHP_EOL;
        $text .= __('DH mile') . ': ' . $this->deadhead_mile . PHP_EOL;
        $text .= __('Authorized miles') . ': ' . $this->authorized_mile . PHP_EOL;

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;

        $text .= __('Total miles') . ': ' . $this->mile . PHP_EOL;

        $text .= PHP_EOL;
        $text .= '------------------------------' . PHP_EOL;
        $text .= PHP_EOL;

        if ($this->driver && $this->driver->telegram_username) {
            $driverUsername = $this->driver->telegram_username;
            if (!Str::startsWith($driverUsername, '@')) {
                $driverUsername = '@' . $driverUsername;
            }
            $text .= $driverUsername;
        }

        return $text;
    }

    public static function thirdPartyTypes()
    {
        return [
            static::TYPE_LOAD_REPOWERED_THIRD_PARTY,
            static::TYPE_LOAD_RECOVERED_THIRD_PARTY,
        ];
    }

    public function isThirdParty()
    {
        return in_array($this->type, static::thirdPartyTypes());
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public static function companyCodes()
    {
        return [
            'CPWP D',
            'CPWP S',
            'BIDH D',
            'BIDH S',
            'OTMV D',
            'OTMV S',
            'AUZBI-D',
            'AUZBI-S',
        ];
    }

    public static function timezones()
    {
        return [
            'EST',
            'EDT',
            'CST',
            'CDT',
            'MST',
            'MDT',
            'PST',
            'PDT',
        ];
    }

    public static function pickupStatuses()
    {
        return [
            'Ontime',
            'Late',
        ];
    }

    public static function pickupQcComments()
    {
        return [
            'Driver error',
            'Dispatch error',
            'Fleet error',
            'Trailer error',
            '-',
        ];
    }

    public static function deliveryStatuses()
    {
        return [
            'Ontime',
            'Late',
        ];
    }

    public static function deliveryQcComments()
    {
        return [
            'Driver error',
            'Dispatch error',
            'Fleet error',
            'Trailer error',
            '-',
        ];
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function getTeamNameAttribute()
    {
        $name = '-';
        if ($this->team) {
            $name = $this->team->name;
        }
        return $name;
    }

    public function getLocalPickupTimeFormattedAttribute()
    {
        return $this->local_pickup_time ? ($this->pickup_timezone . ' ' . Helper::formatDateTime($this->local_pickup_time)) : '-';
    }

    public function getLocalDeliveryTimeFormattedAttribute()
    {
        return $this->local_delivery_time ? ($this->delivery_timezone . ' ' . Helper::formatDateTime($this->local_delivery_time)) : '-';
    }

    public function countTotalMile()
    {
        return !$this->isCancelled() ? $this->authorized_loaded_mile + $this->deadhead_mile + $this->authorized_mile : 0;
    }

    public function countTotalRate()
    {
        return !$this->isCancelled() ? $this->rate : $this->tonu;
    }

    public function countRatePerMile()
    {
        $totalMile = $this->countTotalMile();
        return $totalMile > 0 ? round($this->countTotalRate() / $totalMile, 2) : 0;
    }
}
