<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BotUserInfo extends Model
{
    use HasFactory;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const POSITION_DSP = 'DSP';
    const POSITION_QC = 'QC';
    const POSITION_MANAGER = 'Manager';
    const POSITION_INS = 'Insurance';
    const POSITION_FLT = 'Fleet';
    const POSITION_TRL = 'Trailer';
    const POSITION_EQ = 'Equipment';
    const POSITION_ACCT = 'Accounting';
    const POSITION_SFT = 'Safety';
    const POSITION_ELD = 'Safety ELD';
    const POSITION_IT = 'Information Technologies';
    const POSITION_MKTG = 'Marketing';
    const POSITION_OPS = 'Operations managers';
    const POSITION_HR = 'Human resources';

    const SHIFT_DAY = 'Day';
    const SHIFT_NIGHT = 'Night';

    protected $guarded = [];

    protected $casts = [
        'load_types' => 'array',
        'company_codes' => 'array',
    ];

    protected $table = 'bot_user_info';

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function botUser()
    {
        return $this->belongsTo(BotUser::class, 'user_id');
    }

    public function isFilled()
    {
        return !empty($this->full_name) && !empty($this->position) && !empty($this->shift);
    }

    public function isDsp()
    {
        return $this->position == static::POSITION_DSP;
    }

    public function isQc()
    {
        return $this->position == static::POSITION_QC;
    }

    public function isManager()
    {
        return $this->position == static::POSITION_MANAGER;
    }

    public function isActive()
    {
        return $this->active == static::STATUS_ACTIVE;
    }

    public static function positions()
    {
        return [
            static::POSITION_DSP,
            static::POSITION_QC,
            static::POSITION_MANAGER,
            static::POSITION_INS,
            static::POSITION_FLT,
            static::POSITION_TRL,
            static::POSITION_EQ,
            static::POSITION_ACCT,
            static::POSITION_SFT,
            static::POSITION_ELD,
            static::POSITION_IT,
            static::POSITION_MKTG,
            static::POSITION_OPS,
            static::POSITION_HR,
        ];
    }

    public static function shifts()
    {
        return [static::SHIFT_DAY, static::SHIFT_NIGHT];
    }

    public function scopeDispatchers($query)
    {
        return $query->where('position', static::POSITION_DSP);
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function getStatusTitleAttribute()
    {
        return static::statuses()[$this->active] ?? '-';
    }

    public static function statuses()
    {
        return [
            static::STATUS_INACTIVE => __('No'),
            static::STATUS_ACTIVE => __('Yes'),
        ];
    }

    function getLongNameAttribute() {
        return $this->full_name . ( ($this->botUser && $this->botUser->username) ? ' - @' . $this->botUser->username : '') . ' - ' . $this->position;
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function getTeamNameAttribute()
    {
        $name = '-';
        if ($this->team) {
            $name = $this->team->name;
        }
        return $name;
    }
}
