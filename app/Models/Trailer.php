<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trailer extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function loads()
    {
        return $this->hasMany(Load::class);
    }
}
