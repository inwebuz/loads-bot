<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function botUserInfos()
    {
        return $this->hasMany(BotUserInfo::class);
    }

    public function managers()
    {
        return $this->hasMany(BotUserInfo::class)->where('position', BotUserInfo::POSITION_MANAGER);
    }
}
