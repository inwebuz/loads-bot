<?php

namespace App\TelegramBot\Commands;

use App\Helpers\BotHelper;
use App\Models\BotUserInfo;
use App\Models\Load;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\Update;
use Throwable;

class LoadupdateCommand extends UserCommand
{
    protected $name = 'loadupdate';
    protected $description = 'Load update command';
    protected $usage = '/loadupdate';
    protected $version = '1.0.0';
    protected $private_only = true;
    protected $conversation;

    private $callbackdata;

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $message_id = (string)$message->getMessageId();
        $user = $message->getFrom();
        $user_id = (string)$user->getId();
        $chat = $message->getChat();
        $chat_id = (string)$chat->getId();
        $text = trim($message->getText(true));

        $userInfo = BotHelper::getUserInfo($user_id);

        // check user active
        if (!$userInfo || !$userInfo->isActive()) {
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => BotHelper::t('Your account has not been activated yet'),
                'reply_markup' => StartCommand::getKeyboard($user_id),
            ]);
        }

        // check user filled
        $update = json_decode($this->update->toJson(), true);
        if (!$userInfo || !$userInfo->isFilled()) {
            $update['message']['text'] = '/start';
            return (new StartCommand($this->telegram, new Update($update)))->preExecute();
        }

        // Preparing Response
        $data = [
            'chat_id' => $chat_id,
        ];
        $result = Request::emptyResponse();

        // only DSP can create
        if (!$userInfo->isQc()) {
            $data['text'] = 'You are not QC';
            $data['reply_markup'] = StartCommand::getKeyboard($user_id);
            return Request::sendMessage($data);
        }

        // parse data
        $this->callbackdata = BotHelper::parseCallbackData($text);
        $load = null;
        if (isset($this->callbackdata['loadupdate'])) {
            $id = (int)$this->callbackdata['loadupdate'];
            $load = Load::find($id);
        }

        // check other conversations
        $conversation = new Conversation($user_id, $chat_id);
        if ($conversation->exists() && $conversation->getCommand() != $this->getName()) {
            $conversation->notes = [];
            $conversation->update();
            $conversation->stop();
        }

        //Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        // load error check
        if (empty($notes['id']) && !$load) {
            return Request::emptyResponse();
        } elseif (empty($notes['id']) && $load) {
            $notes['id'] = $load->id;
            $this->conversation->update();
        }

        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }

        // back
        if ($text == BotHelper::t('Button Back')) {
            $state--;
            $text = '';
        } elseif ($text == BotHelper::t('Button Cancel')) {
            $state = -1;
        }

        // cancel request
        if ($state == -1) {
            $notes = [];
            $this->conversation->update();
            $this->conversation->stop();
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => BotHelper::t('Welcome'),
                'reply_markup' => StartCommand::getKeyboard($user_id),
            ]);
        }

        switch ($state) {
            case 0:
                if ($text === '' || !in_array($text, Load::pickupStatuses())) {
                    $notes['state'] = 0;
                    $this->conversation->update();

                    $data['text'] = __('Pickup status');
                    $buttons = [
                        ...array_chunk(Load::pickupStatuses(), 2),
                        [BotHelper::t('Button Back')],
                    ];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['pickup_status'] = $text;
                $text = '';

            // no break
            case 1:
                if ($text === '' || !in_array($text, Load::pickupQcComments())) {
                    $notes['state'] = 1;
                    $this->conversation->update();

                    $data['text'] = __('Pickup QC comment');
                    $buttons = [
                        ...array_chunk(Load::pickupQcComments(), 2),
                        [BotHelper::t('Button Back')],
                    ];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['pickup_qc_comment'] = $text;
                $text = '';

            // no break
            case 2:
                if ($text === '' || !in_array($text, Load::deliveryStatuses())) {
                    $notes['state'] = 2;
                    $this->conversation->update();

                    $data['text'] = __('Delivery status');
                    $buttons = [
                        ...array_chunk(Load::deliveryStatuses(), 2),
                        [BotHelper::t('Button Back')],
                    ];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['delivery_status'] = $text;
                $text = '';

            // no break
            case 3:
                if ($text === '' || !in_array($text, Load::deliveryQcComments())) {
                    $notes['state'] = 3;
                    $this->conversation->update();

                    $data['text'] = BotHelper::t('Delivery QC comment');
                    $buttons = [
                        ...array_chunk(Load::deliveryQcComments(), 2),
                        [BotHelper::t('Button Back')],
                    ];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['delivery_qc_comment'] = $text;
                $text = '';

            // no break
            case 4:
                // check load created before
                $load = Load::find($notes['id']);
                if (!$load) {
                    $notes = [];
                    $this->conversation->update();
                    $this->conversation->stop();
                    return Request::sendMessage([
                        'chat_id' => $chat_id,
                        'text' => BotHelper::t('Welcome'),
                        'reply_markup' => StartCommand::getKeyboard($user_id),
                    ]);
                }
                $load->qc_bot_user_id = $user_id;
                $load->pickup_status = $notes['pickup_status'];
                $load->pickup_qc_comment = $notes['pickup_qc_comment'];
                $load->delivery_status = $notes['delivery_status'];
                $load->delivery_qc_comment = $notes['delivery_qc_comment'];
                $load->save();

                $notes = [];
                $this->conversation->update();
                $this->conversation->stop();

                $data['text'] = BotHelper::t('Load has been updated');
                $data['reply_markup'] = StartCommand::getKeyboard($user_id);
                $result = Request::sendMessage($data);
                break;
        }

        return $result;
    }
}
