<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use App\Helpers\BotHelper;
use App\Helpers\Helper;
use App\TelegramBot\Commands\DriverstatusupdateCommand;
use App\TelegramBot\Commands\LoadcreateCommand;
use App\TelegramBot\Commands\LoadsearchCommand;
use App\TelegramBot\Commands\SosCommand;
use App\TelegramBot\Commands\StartCommand;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

/**
 * Generic message command
 *
 * Gets executed when any type of message is sent.
 */
class GenericmessageCommand extends SystemCommand
{
    protected $name = 'genericmessage';
    protected $description = 'Handle generic message';
    protected $version = '1.1.0';

    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $message_id = (string)$message->getMessageId();
        $chat_id = (string)$message->getChat()->getId();
        $from = $message->getFrom();
        $user_id = (string)$from->getId();
        $text = trim($message->getText(true));

        $update = json_decode($this->update->toJson(), true);

        if ($text === BotHelper::t('Button Home')) {
            $update['message']['text'] = '/start';
            return (new StartCommand($this->telegram, new Update($update)))->preExecute();
        } elseif ($text === BotHelper::t('Button Loads')) {
            $update['message']['text'] = '/loadsearch';
            return (new LoadsearchCommand($this->telegram, new Update($update)))->preExecute();
        } elseif ($text === BotHelper::t('Button New Load')) {
            $update['message']['text'] = '/loadcreate';
            return (new LoadcreateCommand($this->telegram, new Update($update)))->preExecute();
        } elseif ($text === '#sos') {
            $update['message']['text'] = '/sos';
            return (new SosCommand($this->telegram, new Update($update)))->preExecute();
        } elseif (Str::startsWith($text, '#')) {
            return (new DriverstatusupdateCommand($this->telegram, new Update($update)))->preExecute();
        } else {
            //
        }

        //If a conversation is busy, execute the conversation command after handling the message
        $conversation = new Conversation($user_id, $chat_id);

        //Fetch conversation command if it exists and execute it
        if ($conversation->exists() && ($command = $conversation->getCommand())) {
            return $this->telegram->executeCommand($command);
        }

        return Request::emptyResponse();
    }
    // public function execute()
    // {
    //     //If a conversation is busy, execute the conversation command after handling the message
    //     $conversation = new Conversation(
    //         $this->getMessage()->getFrom()->getId(),
    //         $this->getMessage()->getChat()->getId()
    //     );

    //     //Fetch conversation command if it exists and execute it
    //     if ($conversation->exists() && ($command = $conversation->getCommand())) {
    //         return $this->telegram->executeCommand($command);
    //     }

    //     return Request::emptyResponse();
    // }
}
