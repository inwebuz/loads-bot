<?php

namespace App\TelegramBot\Commands;

use App\Helpers\BotHelper;
use App\Helpers\Helper;
use App\Models\BotUserInfo;
use App\Models\Team;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\KeyboardButton;
use Throwable;

/**
 * Start command
 *
 * Gets executed when a user first starts using the bot.
 */
class StartCommand extends UserCommand
{
    protected $name = 'start';
    protected $description = 'Start command';
    protected $usage = '/start';
    protected $version = '2.0.0';
    protected $private_only = true;
    protected $conversation;

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $message_id = (string)$message->getMessageId();
        $user = $message->getFrom();
        $user_id = (string)$user->getId();
        $chat = $message->getChat();
        $chat_id = (string)$chat->getId();
        $text = trim($message->getText(true));

        $userInfo = BotHelper::getUserInfo($user_id);
        if (!$userInfo) {
            $userInfo = BotUserInfo::create([
                'user_id' => $user_id,
            ]);
        }

        // already registered
        if ($userInfo->isFilled()) {
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => BotHelper::t('Welcome'),
                'reply_markup' => self::getKeyboard($user_id),
            ]);
        }

        // Preparing Response
        $data = [
            'chat_id' => $chat_id,
        ];
        $result = Request::emptyResponse();

        //Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }

        // back
        if ($text == BotHelper::t('Button Back')) {
            $notes['state']--;
            $text = '';
        } elseif ($text == BotHelper::t('Button Cancel')) {
            $notes['state'] = -1;
        }

        // cancel request
        if (!empty($notes['state']) && $notes['state'] == -1) {
            $notes = [];
            $this->conversation->update();
            $this->conversation->stop();
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => BotHelper::t('Welcome'),
                'reply_markup' => self::getKeyboard($user_id),
            ]);
        }
        switch ($state) {
            case 0:
                if ($text === '') {
                    $notes['state'] = 0;
                    $this->conversation->update();

                    $data['text'] = BotHelper::t('Your name');
                    $data['reply_markup'] = (new Keyboard([$user->getFirstName() . ' ' . $user->getLastName()]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['full_name'] = $text;
                $text = '';

            // no break
            case 1:
                if ($text === '' || !in_array($text, BotUserInfo::positions())) {
                    $notes['state'] = 1;
                    $this->conversation->update();

                    $data['text'] = BotHelper::t('Position');
                    $buttons = [
                        ...array_chunk(BotUserInfo::positions(), 2),
                        [BotHelper::t('Button Back')],
                    ];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['position'] = $text;
                $text = '';

            // no break
            case 2:
                if ($text === '' || !in_array($text, BotUserInfo::shifts())) {
                    $notes['state'] = 2;
                    $this->conversation->update();

                    $data['text'] = BotHelper::t('Shift');
                    $buttons = [
                        BotUserInfo::shifts(),
                        [BotHelper::t('Button Back')],
                    ];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['shift'] = $text;
                $text = '';

            // no break
            case 3:
                $teams = Team::all();
                $teamNames = $teams->pluck('name')->toArray();
                if ($text === '' || !in_array($text, $teamNames)) {
                    $notes['state'] = 3;
                    $this->conversation->update();
                    $data['text'] = BotHelper::t('Your team');
                    $buttons = array_chunk($teamNames, 4);
                    $buttons[] = [BotHelper::t('Button Back')];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['team_name'] = $text;
                $text = '';

            // no break
            case 4:
                // register success
                $team = Team::where('name', $notes['team_name'])->first();
                $userInfo->update([
                    'full_name' => $notes['full_name'],
                    'position' => $notes['position'],
                    'shift' => $notes['shift'],
                    'team_id' => $team->id ?? null,
                ]);

                $notes = [];
                $this->conversation->update();
                $this->conversation->stop();

                $result = Request::sendMessage([
                    'chat_id' => $chat_id,
                    'text' => BotHelper::t('Welcome'),
                    'reply_markup' => self::getKeyboard($user_id),
                ]);

                // notify admin new user
                Helper::toTelegram('New user registered <a href="' . route('bot-user-info.edit', [$userInfo->id]) . '">Edit</a>', config('services.telegram.admin_chat_id'));
                break;
        }

        return $result;
    }

    public static function getKeyboard($user_id)
    {
        $userInfo = BotHelper::getUserInfo($user_id);
        $buttons = [
            [BotHelper::t('Button Loads')],
        ];
        if ($userInfo && $userInfo->isDsp()) {
            array_unshift($buttons, [BotHelper::t('Button New Load')]);
        }
        return (new Keyboard(...$buttons))
            ->setResizeKeyboard(true)
            ->setOneTimeKeyboard(false)
            ->setSelective(false);
    }
}
